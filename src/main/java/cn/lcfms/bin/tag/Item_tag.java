/*
 * 自定义获取栏目标签
 */
package cn.lcfms.bin.tag;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import cn.lcfms.bin.BaseCache;



public class Item_tag extends TagSupport {
	private static final long serialVersionUID = 1L;
	private int parentId;
	private int markId;
	private String item;

	private List<HashMap<String, Object>> list;
	private int i;

	public int getMarkId() {
		return markId;
	}

	public void setMarkId(int markId) {
		this.markId = markId;
	}

	public int getParentId() {
		return parentId;
	}

	public void setParentId(int parentId) {
		this.parentId = parentId;
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}

	public int doStartTag() throws JspException {
		HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();	
		this.list=BaseCache.itemcache;	
		try {		
			if(this.markId!=0){
				for (int i = 0; i < list.size(); i++) {							
					if ((int) this.list.get(i).get("parentId") == this.getParentId() && (int) this.list.get(i).get("markId") == this.markId) {	
						request.setAttribute(this.item, this.list.get(i));
						this.i = i;
						return EVAL_BODY_INCLUDE;
					}
				}
			}else{
				for (int i = 0; i < list.size(); i++) {				
					if ((int) this.list.get(i).get("parentId") == this.getParentId()) {			
						request.setAttribute(this.item, this.list.get(i));
						this.i = i;
						return EVAL_BODY_INCLUDE;
					}
				}
			}			
		} catch (Exception e) {
			e.printStackTrace();
			return SKIP_BODY;
		}
		return SKIP_BODY;
	}
	public int doAfterBody() throws JspException {
		this.i++;
		HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();
		if(this.markId!=0){
			for (int i = this.i; i < this.list.size(); i++) {
				if ((int) this.list.get(i).get("parentId") == this.getParentId() && (int) this.list.get(i).get("markId") == this.markId) {				
					request.setAttribute(this.item, this.list.get(i));
					return EVAL_BODY_AGAIN;
				}else{
					this.i++;
				}
			}
		}else{
			for (int i = this.i; i < this.list.size(); i++) {
				if ((int) this.list.get(i).get("parentId") == this.getParentId()) {			
					request.setAttribute(this.item, this.list.get(i));
					return EVAL_BODY_AGAIN;
				}else{
					this.i++;
				}
			}
		}		
		return SKIP_BODY;
	}
}
