package cn.lcfms.bin;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.lang.ArrayUtils;
import org.apache.ibatis.session.SqlSession;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import cn.lcfms.bin.core.App;

public class BaseService {
	private static HashMap<String,List<HashMap<String, Object>>> tableinfo=new HashMap<>();	
	private SqlBuild[] builds= {};
	private int buildkey=0;
	private boolean transaction=false;
	private SqlSession session;
	
	public BaseService() {}
	public BaseService(String table) {
		setBuild();
		table=table.trim();
		if(table.indexOf(" ")!=-1) {
			builds[buildkey].alias=table.substring(table.lastIndexOf(" ")+1);
			builds[buildkey].table=table.substring(0,table.indexOf(" "));
		}else {
			builds[buildkey].table=table;
		}	
		setColumn();
	}
	
	public BaseService setTable(String table) {
		setBuild();
		table=table.trim();
		if(table.indexOf(" ")!=-1) {
			builds[buildkey].alias=table.substring(table.lastIndexOf(" ")+1);
			builds[buildkey].table=table.substring(0,table.indexOf(" "));
		}else {
			builds[buildkey].table=table;
		}	
		setColumn();
		return this;
	}
	
	public BaseService sql(String sql) {	
		setBuild();
		builds[buildkey].sql=sql;
		sql=sql.toLowerCase();
		if(sql.startsWith("(")) {
			builds[buildkey].action=sql.substring(1,7);
		}else {
			builds[buildkey].action=sql.substring(0,6);	
		}	
		setParamData();
		if(transaction) {
			this.transactionExec();
		}else {
			this.exec();
		}	
		return this;
	}
	
	public void transaction(TransactionSql tran) {	
		this.transaction=true;
		DefaultTransactionDefinition def = new DefaultTransactionDefinition();
		def.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
		TransactionStatus status = App.getManager().getTransaction(def);
		session = App.getSession(false);
		try {
			tran.doTranSql();			
			App.getManager().commit(status);
		} catch (Exception e) {
			App.getManager().rollback(status);
			buildkey++;	
			e.printStackTrace();
		}finally {
			session.close();
		}
		this.transaction=false;
	}
	
	public interface TransactionSql{
		public void doTranSql();
	}
	
	public SqlBuild[] getSqlBuildList() {
		return builds;
	}
	
	public SqlBuild getSqlBuild() {
		return builds[builds.length-1];
	}
	
	public List<HashMap<String, Object>> getResult() {
		return builds[builds.length-1].result;
	}
	
	public int insert_id() {
		return builds[builds.length-1].insert_id;
	}
	
	public int affected_rows() {
		return builds[builds.length-1].affected_rows;
	}
	/**
	 * 获取主键
	 * @return
	 */
	public String getPrimaryKey(){
		setBuild();
		setColumn();
		return builds[buildkey].primaryKey;
	}
	
	/**
	 * 执行多个字段插入
	 * @param strs 字段名
	 * @return 自增的ID
	 */
	public BaseService insert(String... strs){
		setBuild();
		if(strs.length>0) {
			this.column(strs);
		}
		setColumnInfo();
		builds[buildkey].action="insert";
		builds[buildkey].map.put(getPrimaryKey(),getKeyValue());
		builds[buildkey].buildSql();
		this.sql(builds[buildkey].sql);
		return this;
	}
	/**
	 * 执行一个实体对象插入
	 * @param t 实体对象
	 * @return 自增的ID
	 */
	public <T> BaseService insert(T t){	
		if(t instanceof String) {
			String str=(String)t;
			return this.insert(new String[] {str});
		}
		setBuild();
		setColumnInfo();
		builds[buildkey].action="insert";
		Class<?> beanClass=t.getClass();
		Field[] fields=beanClass.getDeclaredFields();
		Field prikey=null;
		for (int i=0;i<fields.length;i++) {
			fields[i].setAccessible(true);
			try {
				String key=fields[i].getName();
				Object value=fields[i].get(t);	
				if(key.equals(getPrimaryKey())) {
					prikey=fields[i];
				}
				a2:for(int j=0;j<builds[buildkey].column.length;j++){
					String name = builds[buildkey].column[j];	
					if(name.equals(key)) {
						builds[buildkey].map.put(key,value);	
						break a2;
					}
				}		
				
			} catch (Exception e) {
				continue;
			}		
		}	
		builds[buildkey].map.put(getPrimaryKey(),getKeyValue());
		builds[buildkey].buildSql();
		this.sql(builds[buildkey].sql);
		if(builds[builds.length-1].success && prikey!=null) {
			prikey.setAccessible(true);
			try {
				prikey.setInt(t, insert_id());
			} catch (IllegalArgumentException | IllegalAccessException e) {
				e.printStackTrace();
			}
		}
		return this;
	}
	
	public <T> BaseService insert(T t,String filter){
		if(t instanceof String) {
			String str=(String)t;
			return this.insert(new String[]{str,filter});
		}
		setBuild();
		setColumnInfo();
		builds[buildkey].action="insert";
		Class<?> beanClass=t.getClass();
		Field[] fields=beanClass.getDeclaredFields();
		String[] split = filter.split(",");
		for (int i=0;i<fields.length;i++) {			
			fields[i].setAccessible(true);
			try {
				String key=fields[i].getName();
				boolean b=ArrayUtils.contains(split, key);
				if(b){
					continue;
				}
				Object value=fields[i].get(t);	
				a2:for(int j=0;j<builds[buildkey].column.length;j++){
					String name = builds[buildkey].column[j];	
					if(name.equals(key)) {
						builds[buildkey].map.put(key,value);	
						break a2;
					}
				}		
				
			} catch (Exception e) {
				continue;
			}		
		}	
		builds[buildkey].map.put(getPrimaryKey(),getKeyValue());
		builds[buildkey].buildSql();
		this.sql(builds[buildkey].sql);
		return this;
	}
	/**
	 * 修改字段
	 * @param strs
	 * @return
	 */
	public BaseService update(String...strs){
		setBuild();
		if(strs.length>0) {
			this.column(strs);
		}
		setColumnInfo();
		builds[buildkey].action="update";
		builds[buildkey].buildSql();
		this.sql(builds[buildkey].sql);
		return this;
	}	
	
	public <T> BaseService update(T t,String filter){
		if(t instanceof String) {
			String str=(String)t;
			return this.update(new String[]{str,filter});
		}
		setBuild();
		setColumnInfo();
		builds[buildkey].action="update";
		HashMap<String,Object> params=new HashMap<String,Object>();
		Class<?> beanClass=t.getClass();
		Field[] fields=beanClass.getDeclaredFields();
		String[] split = filter.split(",");
		a1:for (int i=0;i<fields.length;i++) {		
			try {
				fields[i].setAccessible(true);			
				String key=fields[i].getName();
				boolean b=ArrayUtils.contains(split, key);
				if(b){
					continue;
				}
				Object value=fields[i].get(t);	
				if(key.equals(getPrimaryKey())) {
					this.where("`"+getPrimaryKey()+"`=#{"+getPrimaryKey()+"}");
					params.put(getPrimaryKey(),value);	
					continue a1;
				}			
				a2:for(int j=0;j<builds[buildkey].column.length;j++){
					String name = builds[buildkey].column[j];	
					if(name.equals(key)) {
						params.put(key,value);	
						break a2;
					}
				}		
				
			} catch (Exception e) {
				continue;
			}		
		}			
		builds[buildkey].params=params;		
		builds[buildkey].buildSql();
		this.sql(builds[buildkey].sql);
		return this;
	}
	/**
	 * 修改一个对象
	 * @param t
	 * @return
	 */
	public <T> BaseService update(T t){
		if(t instanceof String) {
			String str=(String)t;
			return this.update(new String[] {str});
		}
		setBuild();
		setColumnInfo();
		builds[buildkey].action="update";
		HashMap<String,Object> params=new HashMap<String,Object>();
		Class<?> beanClass=t.getClass();
		Field[] fields=beanClass.getDeclaredFields();
		a1:for (int i=0;i<fields.length;i++) {
			fields[i].setAccessible(true);
			try {
				String key=fields[i].getName();
				Object value=fields[i].get(t);	
				if(key.equals(getPrimaryKey())) {
					this.where("`"+getPrimaryKey()+"`=#{"+getPrimaryKey()+"}");
					params.put(getPrimaryKey(),value);	
					continue a1;
				}			
				a2:for(int j=0;j<builds[buildkey].column.length;j++){
					String name = builds[buildkey].column[j];	
					if(name.equals(key)) {
						params.put(key,value);	
						break a2;
					}
				}		
				
			} catch (Exception e) {
				continue;
			}		
		}			
		builds[buildkey].params=params;		
		builds[buildkey].buildSql();
		this.sql(builds[buildkey].sql);
		return this;
	}
	
	public BaseService delete(){
		setBuild();
		setColumnInfo();
		builds[buildkey].action="delete";
		builds[buildkey].buildSql();
		this.sql(builds[buildkey].sql);
		return this;
	}
	
	public BaseService deleteById(int id){
		setBuild();
		setColumnInfo();
		this.where(getPrimaryKey(), id);
		builds[buildkey].action="delete";
		builds[buildkey].buildSql();
		this.sql(builds[buildkey].sql);
		return this;
	}
	
	public List<HashMap<String, Object>> selectList(){
		setBuild();
		setColumnInfo();
		builds[buildkey].action="select";
		builds[buildkey].buildSql();
		this.sql(builds[buildkey].sql);
		return builds[builds.length-1].result;
	}
	
	public HashMap<String, Object> selectMap(){
		setBuild();
		setColumnInfo();
		builds[buildkey].action="select";
		builds[buildkey].buildSql();
		this.sql(builds[buildkey].sql);
		if(builds[builds.length-1].result.size()>0) {
			return builds[builds.length-1].result.get(0);
		}else {
			return new HashMap<>();
		}	
	}
	
	@SuppressWarnings("unchecked")
	public <T> T selectColumn(String column){		
		setBuild();		
		this.column(column);
		builds[buildkey].action="select";
		builds[buildkey].buildSql();
		this.sql(builds[buildkey].sql);
		if(builds[builds.length-1].result.size()>0) {
			HashMap<String, Object> map = builds[builds.length-1].result.get(0);
			if(map.containsKey(column)) {
				return (T)map.get(column);
			}
			return null;
		}else {
			return null;
		}		
	}
	
	public <T> T selectOne(Class<?> cl,int id) {
		try {
			return privateSelectOne(cl,id);
		} catch (IllegalAccessException | InvocationTargetException | InstantiationException e) {
			e.printStackTrace();	
			return null;
		}		
	}
	
	@SuppressWarnings("unchecked")
	private <T> T privateSelectOne(Class<?> cl,int id) throws IllegalAccessException, InvocationTargetException, InstantiationException{
		setBuild();
		setColumnInfo();
		T obj=null;
		if(null==builds[buildkey].table) {
			System.out.println("table为空或多表联查不支持selectOne");
			return obj;
		}
		this.where(getPrimaryKey(), id);
		builds[buildkey].action="select";
		builds[buildkey].buildSql();
		this.sql(builds[buildkey].sql);				
		if(builds[builds.length-1].result.size()>0) {
			HashMap<String, Object> data = builds[builds.length-1].result.get(0);
			obj = (T) cl.newInstance();
			for (int i=0;i<builds[builds.length-1].column.length;i++) {
				String key=builds[builds.length-1].column[i];
				try {				
					Field field=cl.getDeclaredField(key);
					field.setAccessible(true);	
					try{																
						String setKey = "set"+key.replaceFirst(key.substring(0, 1),key.substring(0, 1).toUpperCase());						
						if(!data.containsKey(key) || null==data.get(key)) {
							continue;
						}
						if(data.get(key) instanceof java.sql.Timestamp) {
							data.put(key, ((java.sql.Timestamp)data.get(key)).toString());
						}
						Method method=cl.getMethod(setKey,field.getType());	
						method.invoke(obj, data.get(key));							
					}catch(NoSuchMethodException e){
						System.out.println("属性"+key+"没有set方法");
						continue;
					}catch (IllegalArgumentException e) {
						String str="属性"+field.getName()+"的类型是"+field.getType()+",但数据库查出来的类型是"+data.get(key).getClass()+",两种类型不能匹配,所以这个属性映射失败";
						System.out.println(str);
						continue;
					}
				} catch (NoSuchFieldException e) {
					System.out.println("找不到属性"+key);
					continue;
				}
			}
		}
		return obj;		
	}
	
	public int selectCount(){
		setBuild();
		this.column("count(1) as count");
		builds[buildkey].action="select";
		builds[buildkey].buildSql();
		this.sql(builds[buildkey].sql);
		HashMap<String, Object> map = builds[builds.length-1].result.get(0);
		Long l = (Long) map.get("count");
		return l.intValue();
	}
	/**
	 * @param pagerow 每页显示多少行
	 * @param page 当前页
	 */
	public List<HashMap<String, Object>> selectPage(int pagerow,int page){		
		setBuild();
		setColumnInfo();
		this.limit((page-1)*pagerow, pagerow);	
		builds[buildkey].action="selectPage";
		builds[buildkey].buildSql();
		this.sql(builds[buildkey].sql);
		return builds[builds.length-1].result;
	}
	
	public BaseService where(String where){
		setBuild();
		if(builds[buildkey].where==null){
			builds[buildkey].where=new HashSet<String>();
		}
		builds[buildkey].where.add(where);
		return this;
	}
	
	public BaseService whereOr(String where){
		setBuild();
		if(builds[buildkey].whereOr==null){
			builds[buildkey].whereOr=new HashSet<String>();
		}
		builds[buildkey].whereOr.add(where);
		return this;
	}
	
	public BaseService whereOr(String key,Object value){
		setBuild();
		if(builds[buildkey].whereOr==null){
			builds[buildkey].whereOr=new HashSet<String>();
		}
		builds[buildkey].map.put(key, value);
		key=key+"=#{"+key+"}";	
		builds[buildkey].whereOr.add(key);
		return this;
	}
	
	public BaseService where(String key,Object value){
		setBuild();
		if(builds[buildkey].where==null){
			builds[buildkey].where=new HashSet<String>();
		}
		builds[buildkey].map.put(key, value);
		key=key+"=#{"+key+"}";	
		builds[buildkey].where.add(key);
		return this;
	}
	
	public BaseService where(String column,String act,Object value){
		setBuild();
		if(builds[buildkey].where==null){
			builds[buildkey].where=new HashSet<String>();
		}
		builds[buildkey].map.put(column, value);
		column=column+act+"#{"+column+"}";	
		builds[buildkey].where.add(column);
		return this;
	}
	
	public BaseService whereBetween(String column,Object begin,Object end){
		setBuild();
		if(builds[buildkey].where==null){
			builds[buildkey].where=new HashSet<String>();
		}
		if(begin instanceof String) {
			begin="'"+begin+"'";
		}
		if(end instanceof String) {
			end="'"+end+"'";
		}
		builds[buildkey].where.add(column+" BETWEEN "+begin+" AND "+end);
		return this;
	}
	
	public BaseService whereLike(String column,String like){
		setBuild();
		if(builds[buildkey].where==null){
			builds[buildkey].where=new HashSet<String>();
		}
		builds[buildkey].where.add("`"+column+"` LIKE '%"+like+"%'");
		return this;
	}
	
	public BaseService whereIn(String column,int[] array){
		setBuild();
		if(builds[buildkey].where==null){
			builds[buildkey].where=new HashSet<String>();
		}
		if(array.length==0){
			return this;
		}
		StringBuffer string=new StringBuffer("`"+column+"` IN (");
		for(int i=0;i<array.length;i++){
			string.append(array[i]);
			if(i!=array.length-1){
				string.append(",");
			}
		}
		string.append(")");
		builds[buildkey].where.add(string.toString());
		return this;
	}
	
	public BaseService whereIn(String column,String[] array){
		setBuild();
		if(builds[buildkey].where==null){
			builds[buildkey].where=new HashSet<String>();
		}
		if(array.length==0){
			return this;
		}
		StringBuffer string=new StringBuffer("`"+column+"` IN (");
		for(int i=0;i<array.length;i++){
			string.append("'"+array[i]+"'");
			if(i!=array.length-1){
				string.append(",");
			}
		}
		string.append(")");
		builds[buildkey].where.add(string.toString());
		return this;
	}
	
	/**
	 * 设置字段
	 * @param s
	 * @return
	 */
	public BaseService column(String... s){
		setBuild();
		if(builds[buildkey].column==null){
			builds[buildkey].column=s;
		}else{
			int length=s.length+builds[buildkey].column.length;
			String[] strs=new String[length];
			for(int i=0;i<length;i++){
				if(i<builds[buildkey].column.length){
					strs[i]=builds[buildkey].column[i];
				}else{
					strs[i]=s[i-builds[buildkey].column.length];
				}			
			}
			builds[buildkey].column=strs;
		}	
		return this;
	}
	
	public BaseService order(String order){
		if(order.indexOf("desc")!=-1) {
			order=order.replaceFirst("desc", "DESC");
		}
		if(order.indexOf("asc")!=-1) {
			order=order.replaceFirst("asc", "ASC");
		}
		builds[buildkey].order=order;
		return this;
	}
	
	public BaseService limit(int begin,int end){
		builds[buildkey].limit=begin+","+end;
		return this;
	}
	
	public BaseService groupBy(String groupby) {
		builds[buildkey].groupBy=groupby;
		return this;
	}
	
	public BaseService having(String having) {
		builds[buildkey].having=having;
		return this;
	}
	
	/**
	 * 可以传可变参数,数组,map,list
	 * @param obj
	 * @return
	 */
	public BaseService setData(Object... obj){
		setBuild();
		builds[buildkey].params=obj;
		return this;
	}
	/**
	 * 可以传可变参数,数组,map,list
	 * @param obj
	 * @return
	 */
	public BaseService setData(Map<String, Object> map){
		setBuild();
		builds[buildkey].params=map;
		return this;
	}		
	/**
	 * 可以传可变参数,数组,map,list
	 * @param obj
	 * @return
	 */
	public BaseService setData(List<Object> list){
		setBuild();
		builds[buildkey].params=list;
		return this;
	}	
	public JoinOn leftJoin(String jointable){
		JoinOn on=new JoinOn("LEFT JOIN",jointable.trim());
		return on;
	}
	public JoinOn rightJoin(String jointable){		
		JoinOn on=new JoinOn("RIGHT JOIN",jointable.trim());
		return on;
	}
	public JoinOn innerJoin(String jointable){	
		JoinOn on=new JoinOn("INNER JOIN",jointable.trim());
		return on;
	}
	
	public class JoinOn{
		private String table;
		private String jointype;
		JoinOn(String jointype,String table){
			this.jointype=jointype;
			this.table=table.trim();
		}
		public void on(String on){
			setBuild();
			if(builds[buildkey].join==null){
				builds[buildkey].join=new HashSet<String>();
			}
			builds[buildkey].join.add(jointype+" "+table+" ON "+on);	
		}
	}
	
	/**
	 * 设置字段并格式化
	 */
	public interface ColumnFormat {
		public abstract Object format(Object object);
	}
	public BaseService column(String s,ColumnFormat format){
		if(builds[buildkey].column==null){
			builds[buildkey].column=new String[1];
			builds[buildkey].column[0]=s;
		}else{
			int length=builds[buildkey].column.length+1;
			String[] strs=new String[length];
			for(int i=0;i<length;i++){
				if(i<builds[buildkey].column.length){
					strs[i]=builds[buildkey].column[i];
				}else{
					strs[i]=s;
				}			
			}
			builds[buildkey].column=strs;
		}	
		if(null==builds[buildkey].formats){
			builds[buildkey].formats=new HashMap<>();
		}
		if(s.indexOf(" ")!=-1){	
			String sq=s.substring(s.lastIndexOf(" ")+1);	
			builds[buildkey].formats.put(sq, format);
		}else{
			builds[buildkey].formats.put(s,format);
		}	
		return this;
	}
	/**
	 * 格式化查询结果
	 * @param list
	 */
	private void dataFormat(List<HashMap<String, Object>> list){
		if(list.size()>0 && builds[buildkey].formats!=null){
			for(int i=0;i<list.size();i++){
				HashMap<String, Object> rows = list.get(i);
				Set<Entry<String, Object>> set = rows.entrySet();
				Iterator<Entry<String, Object>> iter = set.iterator();					
				while(iter.hasNext()){
					Entry<String, Object> next = iter.next();
					String key = next.getKey();						
					if(builds[buildkey].formats.containsKey(key)){
						Object value=next.getValue();
						ColumnFormat columnDataFormat = builds[buildkey].formats.get(key);
						rows.put(key,columnDataFormat.format(value));
					}
				}
			}
		}
	}
	private void setParamData(){
		String regex="#\\{\\w*?\\}";
		Pattern pattern=Pattern.compile(regex);
		Matcher matcher = pattern.matcher(builds[buildkey].sql);
		int i=0;
		while(matcher.find()){
			String p=matcher.group();
			p=p.substring(2, p.length()-1);
			if(builds[buildkey].map.containsKey(p))continue;
			if(builds[buildkey].params instanceof Object[]){
				Object[] paramArr=(Object[])builds[buildkey].params;
				builds[buildkey].map.put(p, paramArr[i]);			
			}else if(builds[buildkey].params instanceof List){
				List<?> list=(List<?>) builds[buildkey].params;
				builds[buildkey].map.put(p, list.get(i));
			}else if(builds[buildkey].params instanceof Map){
				Map<?,?> m=(Map<?,?>) builds[buildkey].params;
				builds[buildkey].map.put(p, m.get(p));
			}
			i++;			
		}		
	}
	/**
	 * 返回字段信息
	 * @param table
	 * @return
	 */
	private void setColumnInfo(){
		String table=builds[buildkey].table;
		if(null==table)return;
		if(null==builds[buildkey].column){
			List<HashMap<String, Object>> info = tableinfo.get(table);
			String[] r=new String[info.size()];
			for(int i=0;i<info.size();i++){
				String name = (String) info.get(i).get("COLUMN_NAME");
				r[i]=name;		
			}
			builds[buildkey].column=r;
		}
	}
	/*
	 * 获取某表的主键的值
	 */
	public synchronized int getKeyValue() {	
		List<HashMap<String, Object>> list = tableinfo.get(builds[buildkey].table);
		for(HashMap<String, Object> map:list) {
			String key=(String) map.get("COLUMN_KEY");
			if(key.equals("PRI")) {					
				int value = (int) map.get("KEY_VALUE");				
				value++;
				map.put("KEY_VALUE", value);
				return value;
			}
		}
		return 1;
	}
	
	private void setBuild() {
		int length=builds.length;
		if(buildkey>length-1) {
			SqlBuild[] sd=new SqlBuild[length+1];
			for(int i=0;i<builds.length;i++) {
				sd[i]=builds[i];
			}
			sd[length]=new SqlBuild();			
			builds=sd;
		}
		if(null==builds[buildkey].table) {
			for(int i=builds.length-1;i>=0;i--) {
				if(null!=builds[i].table) {
					builds[buildkey].table=builds[i].table;
					builds[buildkey].alias=builds[i].alias;
					break;
				}
			}
		}
	}
	/**
	 * 获取主键
	 */
	private void setColumn(){	
		String table=builds[buildkey].table;
		if(null==table)return;
		if(BaseService.tableinfo.get(table)==null){
			SqlSession session=App.getSession(true);
			BaseDao mapper = session.getMapper(BaseDao.class);	
			HashMap<String,Object> map=new HashMap<>();
			String url=(String) App.APPCONFIG.get("jdbc.url");
			url=url.substring(0, url.indexOf("?"));
			String dbname=url.substring(url.lastIndexOf("/")+1);			
			map.put("sql", "select COLUMN_NAME,COLUMN_KEY,COLUMN_TYPE from information_schema.columns where TABLE_NAME='"+table+"' and TABLE_SCHEMA='"+dbname+"'");
			List<HashMap<String, Object>> list= mapper.selectList(map);     
			BaseService.tableinfo.put(table, list);
			session.close();
		}
		List<HashMap<String, Object>> list =BaseService.tableinfo.get(table);
		boolean b=false;
		for(int i=0;i<list.size();i++){
			if(!((String)list.get(i).get("COLUMN_TYPE")).startsWith("int")){
				continue;
			}	
			if(list.get(i).get("COLUMN_KEY").equals("PRI")){
				builds[buildkey].primaryKey=(String)list.get(i).get("COLUMN_NAME");
			}
			if(list.get(i).get("COLUMN_KEY").equals("PRI") && !list.get(i).containsKey("KEY_VALUE")){			
				SqlSession session=App.getSession(true);
				BaseDao mapper = session.getMapper(BaseDao.class);	
				HashMap<String,Object> map=new HashMap<>();
				map.put("sql", "select max("+builds[buildkey].primaryKey+") as mv from `"+table+"`");
				List<HashMap<String, Object>> maxlist= mapper.selectList(map); 
				if(null==maxlist.get(0)) {
					list.get(i).put("KEY_VALUE",0);
				}else {
					int maxvalue=(int) maxlist.get(0).get("mv");
					list.get(i).put("KEY_VALUE", maxvalue);
				}
				session.close();
				b=true;
			}	
			if(list.get(i).containsKey("KEY_VALUE")) {
				b=true;
				break;
			}			
		}
		if(!b) {
			try {
				throw new Exception(table+"结构异常,没有找到int类型主键,查看表引擎是否是InnoDB,或者没有设置主键,不能使用BaseService");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private void transactionExec() {
		builds[buildkey].success=false;
		BaseDao mapper = session.getMapper(BaseDao.class);	
		builds[buildkey].map.put("sql", builds[buildkey].sql);
		switch (builds[buildkey].action) {
		case "select":
			List<HashMap<String, Object>> list = mapper.selectList(builds[buildkey].map);
			dataFormat(list);
			builds[buildkey].result=list;	
			break;	
		case "update":	
			int n=mapper.update(builds[buildkey].map);
			builds[buildkey].affected_rows=n;
			break;
		case "delete":	
			int l=mapper.delete(builds[buildkey].map);
			builds[buildkey].affected_rows=l;
			break;
		case "insert":	
			int i=mapper.insert(builds[buildkey].map);
			builds[buildkey].affected_rows=i;
			builds[buildkey].insert_id=(int)builds[buildkey].map.get(getPrimaryKey());
			break;
		default:
			break;
		}	
		builds[buildkey].success=true;
		buildkey++;	
	}
	
	private void exec() {
		session=App.getSession(false);
		try {			
			BaseDao mapper = session.getMapper(BaseDao.class);	
			builds[buildkey].map.put("sql", builds[buildkey].sql);
			switch (builds[buildkey].action) {
			case "select":
				List<HashMap<String, Object>> list = mapper.selectList(builds[buildkey].map);
				dataFormat(list);
				builds[buildkey].result=list;	
				break;
			case "selectPage":
				List<HashMap<String, Object>> pagelist = mapper.selectList(builds[buildkey].map);
				dataFormat(pagelist);
				builds[buildkey].result=pagelist;
				break;
			case "update":	
				int n=mapper.update(builds[buildkey].map);
				builds[buildkey].affected_rows=n;
				break;
			case "delete":	
				int l=mapper.delete(builds[buildkey].map);
				builds[buildkey].affected_rows=l;
				break;
			case "insert":	
				int i=mapper.insert(builds[buildkey].map);
				builds[buildkey].affected_rows=i;
				builds[buildkey].insert_id=(int)builds[buildkey].map.get(getPrimaryKey());
				break;
			default:
				break;
			}	
			session.commit();
			builds[buildkey].success=true;
		} catch (Exception e) {
			e.printStackTrace();
			builds[buildkey].success=false;			
		} finally {	
			buildkey++;	
			session.close();
		}
	}		
}
