package cn.lcfms.bin;

import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import cn.lcfms.bin.annotation.PermitPoll;
import cn.lcfms.bin.core.App;

public class Permission {	
	public static List<HashMap<String, Object>> PERMIT;//权限列表
	private static Permission ts1 = null;
	
	private static Map<Integer, HashSet<Integer>> group_permission=new HashMap<Integer, HashSet<Integer>>();
	public static Permission getpermission(){
		if (ts1 == null) {
				ts1 = new Permission();							
		}
		return ts1;
	}
    public static Map<Integer, HashSet<Integer>>  get_group_permission(){
    	return Permission.group_permission;
    }

	public static void setGroup_permission(Map<Integer, HashSet<Integer>> group_permission) {
		Permission.group_permission = group_permission;
	}
	/**
	 * 控制器访问权限设置
	 * 
	 * @param action_name
	 *            控制器所在位置
	 * @param perm
	 *            用户组下的权限列表
	 */
	public boolean if_permisson(HttpServletRequest request){
		if(null==Userinfo.getUserInfo("gid", request)) {
			return false;
		}
		int groupId=Userinfo.getUserInfo("gid", request);
		int perActionID=Userinfo.getUserInfo("perActionID", request);						
		HashSet<Integer> set=Permission.group_permission.get(groupId);
		if(set.contains(perActionID)){
			return true;
		}	
		return false;		
	}
    /**
     * 加载控制器权限
     * @throws ClassNotFoundException 
     * @throws SQLException 
     */
    public void initialize() throws ClassNotFoundException, SQLException{   		
    	BaseService service=App.getService("permit_action");
    	ArrayList<String> filelist=App.getControllerFile();
    	Iterator<String> iterator=filelist.iterator();
    	while(iterator.hasNext()){
    		String classname=iterator.next();
    		Class<?> cls=Class.forName(classname);
    		if(cls.isAnnotationPresent(PermitPoll.class)==true){
             	 PermitPoll name = cls.getAnnotation(PermitPoll.class);
             	 HashMap<String, Object> map=new HashMap<String, Object>();             	
             	 map.put("action", classname);
             	 map.put("type", name.type());
             	 map.put("name", name.name());
             	 map.put("forward", name.forward()); 
             	 int id;  
             	 service.where("action=#{action}");
             	 service.setData(classname);
             	 List<HashMap<String, Object>> list = service.selectList();
             	 if(list.size()==0){
             		 service.setData(name.type(),name.name(),classname,name.forward());
             		 service.insert("type","name","action","forward");
             		 id=service.insert_id();
                 }else{
                	 service.setData(name.type(),name.name(),name.forward(),classname);
                	 service.where("action=#{action}");
                	 service.update("type","name","forward");
                	 id=(int) list.get(0).get("id");
                 }
             	 map.put("id", id);
             	 PERMIT.add(map);
            } 
    		Method[] methods = cls.getMethods();
            for (Method method : methods) {            	
                 if(method.isAnnotationPresent(PermitPoll.class)==true){
                 	 PermitPoll name = method.getAnnotation(PermitPoll.class);
                 	 HashMap<String, Object> map=new HashMap<String, Object>();
                 	 map.put("action", classname+"."+method.getName());
                 	 map.put("type", name.type());
                 	 map.put("name", name.name());
                 	 map.put("forward", name.forward());              	 
                 	 int id;
                 	 service.setData(classname+"."+method.getName()).sql("select * from permit_action where action=#{action}");                 	
                 	 List<HashMap<String, Object>> list = service.getResult();
                 	 if(list.size()==0){
                 		 service.setData(name.type(),name.name(),classname+"."+method.getName(),name.forward());
                 		 service.insert("type","name","action","forward");
                 		 id=service.insert_id();
                     }else{
                    	 service.setData(name.type(),name.name(),name.forward(),classname+"."+method.getName());
                    	 service.where("action=#{action}");
                    	 service.update("type","name","forward");
                    	 id=(int) list.get(0).get("id");
                     }
                 	 map.put("id", id);
                 	 PERMIT.add(map);
                 }             
            }
    	}   
    	String ids="(-1,";
    	for(int i=0;i<PERMIT.size();i++){
    		ids+=PERMIT.get(i).get("id")+",";
    	}
    	ids=ids.substring(0, ids.length()-1)+")"; 
    	service.sql("delete from permit_action where id not in "+ids);
	} 
    
    /**
     * 加载控制器权限
     * @throws ClassNotFoundException 
     * @throws SQLException 
     */
    public void debugInitialize() throws ClassNotFoundException, SQLException{   		
    	BaseService service=App.getService("permit_action");
    	ArrayList<String> filelist=App.getControllerFile();
    	Iterator<String> iterator=filelist.iterator();
    	while(iterator.hasNext()){
    		String classname=iterator.next();
    		Class<?> cls=Class.forName(classname);
    		if(cls.isAnnotationPresent(PermitPoll.class)==true){
             	 PermitPoll name = cls.getAnnotation(PermitPoll.class);
             	 HashMap<String, Object> map=new HashMap<String, Object>();             	
             	 map.put("action", classname);
             	 map.put("type", name.type());
             	 map.put("name", name.name());
             	 map.put("forward", name.forward()); 
             	 service.where("action=#{action}");
             	 service.setData(classname);
             	 int id=service.selectColumn("id");
             	 map.put("id", id);
             	 PERMIT.add(map);
            } 
    		Method[] methods = cls.getMethods();
            for (Method method : methods) {            	
                 if(method.isAnnotationPresent(PermitPoll.class)==true){
                 	 PermitPoll name = method.getAnnotation(PermitPoll.class);
                 	 HashMap<String, Object> map=new HashMap<String, Object>();
                 	 map.put("action", classname+"."+method.getName());
                 	 map.put("type", name.type());
                 	 map.put("name", name.name());
                 	 map.put("forward", name.forward());              	 
                 	 service.setData(classname+"."+method.getName());                 	
                     service.where("action=#{action}");
                     int id=service.selectColumn("id");
                 	 map.put("id", id);
                 	 PERMIT.add(map);
                 }             
            }
    	}   
	} 
	/**
	 * 加载权限数据到内存
	 */
    public void set_permission() throws Exception{ 
    	BaseService service = App.getService("group");
    	List<HashMap<String, Object>> list=service.selectList();
		this.set_group_permission(list);
    }
	/**
	 * 将数据库组权限加载到内存中
	 * @throws Exception 
	 */
	public void set_group_permission(List<HashMap<String, Object>> list) throws Exception {			
		Map<Integer, HashSet<Integer>> map=new HashMap<Integer, HashSet<Integer>>();
		for(int i=0;i<list.size();i++){		
			int groupId=(int) list.get(i).get("gid");
			String groupS=(String)list.get(i).get("gpermission");
			String[] garr=groupS.split(",");
			HashSet<Integer> set=new HashSet<Integer>();
			for(int j=0;j<garr.length;j++){
				set.add(Integer.valueOf(garr[j]));
			}	
			map.put(groupId,set);
		}		
		Permission.group_permission=map;
	}
	
}
