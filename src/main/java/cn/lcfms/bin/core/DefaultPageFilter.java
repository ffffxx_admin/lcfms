package cn.lcfms.bin.core;

import java.io.CharArrayWriter;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

import com.googlecode.htmlcompressor.compressor.HtmlCompressor;

import cn.lcfms.bin.Userinfo;
import cn.lcfms.bin.annotation.StaticHtml;

public class DefaultPageFilter implements Filter {
	private static Logger logger = Logger.getLogger(DefaultPageFilter.class); 
	private static List<HashMap<String, Object>> HTML=new ArrayList<>();//生成静态列表
	private static HtmlCompressor compressor=new HtmlCompressor();
	private FilterConfig config;

	
	private class ResponseWrapper extends HttpServletResponseWrapper {  
	    private PrintWriter cachedWriter;  
	    private CharArrayWriter bufferedWriter;     
	    public ResponseWrapper(HttpServletResponse response) {  
	        super(response);  
	        bufferedWriter = new CharArrayWriter();  
	        cachedWriter = new PrintWriter(bufferedWriter);  
	    }  	   
	    @Override  
	    public PrintWriter getWriter() {  
	        return cachedWriter;  
	    }  
	    public String getContent() {  
	        return bufferedWriter.toString();  
	    }  
	}  
	
	@Override
	public void destroy() {
		logger.info("过滤器销毁");
	}

	@Override
	public void doFilter(ServletRequest r1, ServletResponse r2,FilterChain chain) throws IOException, ServletException {						
		HttpServletRequest request = (HttpServletRequest) r1;
		HttpServletResponse response = (HttpServletResponse) r2;
		Userinfo.putUserInfo("TIMESTART", new Date().getTime(), request);
		Userinfo.putUserInfo("MEMORYSTART", Runtime.getRuntime().freeMemory(), request);
		String uri=request.getRequestURI();
		String contextPath = request.getContextPath();
		String path=uri.substring(contextPath.length());
		if(!path.equals("/") && path.endsWith("/")){
			response.sendRedirect(uri.substring(0, uri.length()-1));			
			return;
		}
		if(path.indexOf(".html") !=-1 || path.indexOf(".ico") !=-1 || path.startsWith("/statics") || path.startsWith("/uploadfile")){
			chain.doFilter(request, response);
			return;
		}
		if(path.equals("/websocket/getSocketConnct")){	
			chain.doFilter(request, response);
			return;
		}
		logger.info("进入路由解析器");
		logger.info(path);
		ResponseWrapper wrapper=new ResponseWrapper(response);		
		@SuppressWarnings("unchecked")
		Enumeration<String> initParameterNames = config.getInitParameterNames();
		while(initParameterNames.hasMoreElements()){
			String name=initParameterNames.nextElement();
			if(path.equals(name)){
				String url=config.getInitParameter(name);
				RequestDispatcher dispatcher = request.getRequestDispatcher(url);
				setMca(url,request);
				dispatcher.forward(request, wrapper);
				endApp(request,response,wrapper);
				return;
			}
		}	
		setMca(path,request);	 
        chain.doFilter(request,wrapper);   
        endApp(request,response,wrapper);
	}
	
	private void endApp(HttpServletRequest request,HttpServletResponse response,ResponseWrapper wrapper) throws IOException {
    	String html = (String) Userinfo.getUserInfo("html", request);
    	String content=wrapper.getContent();   	
    	if(null!=html) {    
    		content=compileToHtml(content);
        	FileUtils.writeStringToFile(new File(html), content, "utf8");
    	}
    	if(!wrapper.isCommitted()) { 		
    		PrintWriter writer = response.getWriter();
    		writer.write(content);
    		writer.flush();
    		writer.close();
    	}
   		App.printEnd(request);
	}
	
	private String compileToHtml(String html) {
		String compressedHtml = compressor.compress(html);
		return compressedHtml;
	}

	@Override
	public void init(FilterConfig config) throws ServletException {
		this.config=config;				
		ArrayList<String> filelist=App.getControllerFile();
    	Iterator<String> iterator=filelist.iterator();
    	Date date=new Date();
    	while(iterator.hasNext()){
    		String classname=iterator.next();
    		Class<?> cls = null;
			try {
				cls = Class.forName(classname);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
    		Method[] methods = cls.getMethods();
            for (Method method : methods) {            	
                 if(method.isAnnotationPresent(StaticHtml.class)==true){
                	 StaticHtml name = method.getAnnotation(StaticHtml.class);
                 	 HashMap<String, Object> map=new HashMap<String, Object>();
                 	 int time = name.time();
                 	 String[] param=name.param();
                 	 String file=name.file();
                 	 map.put("action", classname+"."+method.getName());
                 	 map.put("time", time);
                 	 map.put("param", param);
                 	 map.put("file", file);
                 	 map.put("last", date.getTime());
                 	 HTML.add(map);
                 }                
            }
    	}   
	}
	
	public static HashMap<String, Object> html(String m,String c,String a,HttpServletRequest request) {
		String f=File.separator;
		HashMap<String, Object> result=new HashMap<>();
		for(HashMap<String, Object> map:HTML) {
			String action = (String) map.get("action");
			String mca="cn.lcfms.app."+m+".controller."+c+"Controller."+a;
			if(action.equals(mca)) {
				String file = (String) map.get("file");
				String[] param = (String[]) map.get("param");					
				if(file.startsWith("/")) {
					file=file.substring(1);
				}
				if(file.endsWith("/")) {
					file=file.substring(0,file.length()-1);
				}				
				if(file.equals("")) {
					StringBuffer sBuffer=new StringBuffer(); 
					for(int i=0;i<param.length;i++) {
						String p=request.getParameter(param[i]);
						if(null==p || p.equals("")) {
							p=param[i];											
						}
						if(p.length()>10) {
							p=p.substring(0, 10);
						}
						sBuffer.append(p);	
						if(i!=param.length-1) {
							sBuffer.append(f);
						}				
					}			
					if(sBuffer.length()==0) {
						result.put("file", m+f+c+f+a+".html");	
					}else {
						result.put("file", m+f+c+f+a+f+sBuffer.toString()+".html");	
					}						
				}else {	
					file=replaceUserinfoParam(file,file,request);
					for(int i=0;i<param.length;i++) {		
						String p=request.getParameter(param[i]);
						if(null==p || p.equals("")) {
							p=param[i];											
						}	
						if(p.length()>10) {
							p=p.substring(0, 10);
						}						
						file=file.replace("${"+param[i]+"}", p);
						
					}				
					result.put("file", file);							
				}
				
				Date date=new Date();
				long now=date.getTime();
				int time=(int) map.get("time");
				long last=(long	) map.get("last");
				if((last+time*1000)<now){
					map.put("last", now);
					result.put("overdue", true);
				}else {
					result.put("overdue", false);
				}								
				return result;
			}
		}
		return null;
	}	
    
	private static String replaceUserinfoParam(String file,String result,HttpServletRequest request) {
		if(file.indexOf("${USERINFO.")!=-1) {
			String ma=file.substring(file.indexOf("${USERINFO."));
			String l=ma.substring(11,ma.indexOf("}"));
			String n=file.substring(file.indexOf("${USERINFO."),file.indexOf("}")+1);
			result=result.replace(n, String.valueOf(Userinfo.getUserInfo(l, request)));
			ma=ma.substring(ma.indexOf("}")+1,ma.length());			
			return replaceUserinfoParam(ma,result,request);
		}else {
			return result;
		}
	}
	
	private static void setMca(String uri,HttpServletRequest request){
		String[] split = uri.substring(1).split("/");
		if(split.length<3){
			logger.info("请求的路由不存在");
			return;
		}
		split[1]=split[1].replaceFirst(split[1].substring(0, 1),split[1].substring(0, 1).toUpperCase());		
		Userinfo.putUserInfo("mca", split, request);
		logger.info("模块名："+split[0]+" 控制器："+split[1]+"Controller 方法名："+split[2]);
	}	
}
