package cn.lcfms.bin.core;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.lcfms.bin.BaseService;

public class CmsDataBase {	
	protected HttpServletRequest request;
	protected HttpServletResponse response;
	protected int id;
	protected BaseService service;
	public CmsDataBase(HttpServletRequest request,HttpServletResponse response,int id,BaseService service) {
		this.request=request;
		this.response=response;
		this.id=id;
		this.service=service;
	}
}
