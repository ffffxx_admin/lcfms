/*
 * 广播处理器
 * 作者：成绍勇
 * 2015-11-9
 */
package cn.lcfms.bin;

import java.io.IOException;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.RemoteEndpoint;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import cn.lcfms.bin.core.App;
import cn.lcfms.utils.Encrypt;

@ServerEndpoint("/websocket/getSocketConnct")
public class Broadcast {
	private String username;
	private int userId;
	private String id;
    public static Map<String,BroadcastSession> sessionMap = new Hashtable<>();
 
    class BroadcastSession{
    	private int userId;
    	private String userName;
    	private Session session;
    	BroadcastSession(Session session,int userId,String userName){
    		this.session=session;
    		this.userId=userId;
    		this.userName=userName;
    	}
		public Session getSession() {
			return session;
		}
		public int getUserId() {
			return userId;
		}
		public String getUserName() {
			return userName;
		}
    	
    }
    @OnOpen
    public void start(Session session) throws InterruptedException, IOException{ 
    	this.username=session.getRequestParameterMap().get("aname").get(0);  
    	this.userId=Integer.valueOf(session.getRequestParameterMap().get("aid").get(0));  
    	this.id=session.getRequestParameterMap().get("id").get(0); 
    	Set<Entry<Long, int[]>> set=App.BROADCAST_ID.entrySet();
		Iterator<Entry<Long, int[]>> iterator=set.iterator();
		while (iterator.hasNext()) {
			Entry<Long, int[]> i=iterator.next();
			int[] val=i.getValue();
			Encrypt md5=Encrypt.getEncrypt();
			if(md5.md5(this.userId+this.username+val[0]).equals(this.id)){
				 sessionMap.put(session.getId(),new BroadcastSession(session,this.userId, this.username));
				 System.out.println("用户："+this.username  + "，接入广播列表..");
				 return;
			}else if(md5.md5(this.userId+this.username+val[1]).equals(this.id)){
				 sessionMap.put(session.getId(),new BroadcastSession(session,this.userId, this.username));        
				 System.out.println("用户："+this.username  + "，接入广播列表..");
				 return;
			}						
		}	
		System.out.println("id错误,禁止使用广播！");
		session=null;
    }
 
    @OnMessage
    public void process(Session session, String message) throws IOException{
    	System.out.println(message);
    	echo(this.username +" [说]: "+message);
    }
 
 
    @OnClose
    public void end(Session session) throws IOException{
        sessionMap.remove(session.getId());
        System.out.println("用户"+this.username  + "退出广播列表..");
    }
 
    @OnError
    public void error(Session session, java.lang.Throwable throwable) throws IOException{
        end(session);
    }
    /**
     * 广播给单个用户
     * @param userName
     * @param message
     * @throws IOException 
     */
    public static void echo(String userName,Object message) throws IOException{
        RemoteEndpoint.Basic remote = null;
        Set<Map.Entry<String,BroadcastSession>> set = sessionMap.entrySet();
        for(Map.Entry<String,BroadcastSession> i: set){
            remote = i.getValue().getSession().getBasicRemote();
            String name = i.getValue().getUserName();
            if(name.equals(userName)){
                remote.sendText(message.toString());
            }
        }
    }
    /**
     * 广播给多个用户
     * @param userName
     * @param message
     * @throws IOException 
     */
    public static void echo(String[] userName,Object message) throws IOException{
    	 RemoteEndpoint.Basic remote = null;
         Set<Map.Entry<String,BroadcastSession>> set = sessionMap.entrySet();
         for(Map.Entry<String,BroadcastSession> i: set){
             remote = i.getValue().getSession().getBasicRemote();
             String name = i.getValue().getUserName();
             for(String u:userName){
            	 if(u.equals(name)){
                     remote.sendText(message.toString()); 
            	 }
             }
         }
    }
    /**
     * 广播给单个用户
     * @param userId
     * @param message
     * @throws IOException 
     */
    public static void echo(int userId,Object message) throws IOException{
        RemoteEndpoint.Basic remote = null;
        Set<Map.Entry<String,BroadcastSession>> set = sessionMap.entrySet();
        for(Map.Entry<String,BroadcastSession> i: set){
            remote = i.getValue().getSession().getBasicRemote();
            int id = i.getValue().getUserId();
            if(id==userId){
                remote.sendText(message.toString());
            }
        }
    }
    /**
     * 广播给多个用户
     * @param userId
     * @param message
     * @throws IOException 
     */
    public static void echo(int[] userId,Object message) throws IOException{
    	 RemoteEndpoint.Basic remote = null;
         Set<Map.Entry<String,BroadcastSession>> set = sessionMap.entrySet();
         for(Map.Entry<String,BroadcastSession> i: set){
             remote = i.getValue().getSession().getBasicRemote();
             int id = i.getValue().getUserId();
             for(int u:userId){
            	 if(u==id){
                     remote.sendText(message.toString()); 
            	 }
             }
         }
    }
    /**
     * 广播给所有在线的用户
     * @param message
     * @throws IOException 
     */
    public static void echo(Object message) throws IOException{
    	 RemoteEndpoint.Basic remote = null;
         Set<Map.Entry<String,BroadcastSession>> set = sessionMap.entrySet();
         for(Map.Entry<String,BroadcastSession> i: set){
             remote = i.getValue().getSession().getBasicRemote();        
             remote.sendText(message.toString()); 
         }
    }
}