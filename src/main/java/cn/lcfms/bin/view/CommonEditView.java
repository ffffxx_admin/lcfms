package cn.lcfms.bin.view;



import java.util.HashMap;
import java.util.HashSet;
import javax.servlet.http.HttpServletRequest;



public class CommonEditView extends CommonEdit{
	private String title;
	public CommonEditView(String title){
		this.setViewName("admin/table/editpage");
		this.addObject("CommonPage", this);
		this.title=title;
	}
	public String getTitle() {
		return title;
	}
	
	/**
	 * 设置地理城市选择类型
	 * @param title 标题
	 * @param level 级数，一级只显示省份,二级显示省份跟地区,三级显示县和区
	 */
	public void setAreasForm(String title,int level){
		if(form==null){
			form=new HashSet<>();
		}
		HashMap<String, Object> map=new HashMap<String, Object>();
		map.put("title", title);
		map.put("level", level);
		map.put("type", "areas");
		form.add(map);
		int size = form.size();
		map.put("sort", size-1);
	}
	/**
	 * 设置地理城市选择类型
	 * @param title 标题
	 * @param level 级数，一级只显示省份,二级显示省份跟地区,三级显示县和区
	 * @param defaultValue 默认值，数组的长度必须与level保持一致
	 */
	public void setAreasForm(String title,int level,int[] defaultValue){
		if(form==null){
			form=new HashSet<>();
		}
		HashMap<String, Object> map=new HashMap<String, Object>();
		map.put("title", title);
		map.put("level", level);
		map.put("type", "areas");
		int[] d=new int[4];
		for(int i=0;i<defaultValue.length;i++) {
			d[i]=defaultValue[i];
		}
		map.put("defaultValue", d);
		form.add(map);
		int size = form.size();
		map.put("sort", size-1);
	}
	
	@Override
	public void setPage(HttpServletRequest request){
		request.setAttribute("form", form);
		request.setAttribute("action", action);
		request.setAttribute("title", title);
		request.setAttribute("method", method);
	}
}
