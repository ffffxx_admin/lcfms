package cn.lcfms.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeUtils {
	
	public enum Formatter{
		SHORT("yyyyMMdd"),_SHORT("yyyy-MM-dd"),LONG("yyyy-MM-dd HH:mm:ss");	
		private String time;
		public String getTime() {
			return this.time;
		}
		Formatter(String pattern) {
			SimpleDateFormat sdf=new SimpleDateFormat(pattern);
			Date date = new Date();
			time = sdf.format(date);
		}		
	}
	
	public static String getCurrentDateTime() {
		return Formatter._SHORT.getTime();
	}
	
	public static String getCurrentDateTime(Formatter formatter) {	
		return formatter.getTime();
	}
	
	public static String getCurrentTime() {
		 long time = new Date().getTime();
		 return String.valueOf(time);
	}
	
	public static String getCurrentShortTime() {
		 long time = new Date().getTime();
		 return String.valueOf(time/1000);
	}
	
	public static String getFormatTime(Formatter formatter,String time) {
		SimpleDateFormat sdf=new SimpleDateFormat(formatter.getTime());
		Date date = new Date(Long.valueOf(time));
		String r = sdf.format(date);
		return r;
	}
	
	public static void main(String[] args) {
		
	}
	
}
