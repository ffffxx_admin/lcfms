package cn.lcfms.app.weixin.util;


public class WeixinMsg {
	private String ToUserName;
	private String FromUserName;
	private String CreateTime;
	private String Content;

	public void setToUserName(String toUserName) {
		ToUserName = toUserName;
	}

	public void setFromUserName(String fromUserName) {
		FromUserName = fromUserName;
	}

	public void setCreateTime(String createTime) {
		CreateTime = createTime;
	}

	public void setContent(String content) {
		Content = content;
	}

	public String textMsg() {
		String str="<xml>\n"
				+ "<ToUserName><![CDATA["+ToUserName+"]]></ToUserName>\n"
				+ "<FromUserName><![CDATA["+FromUserName+"]]></FromUserName>\n"
				+ "<CreateTime>"+CreateTime+"</CreateTime>\n"
				+ "<MsgType><![CDATA[text]]></MsgType>\n"
				+ "<Content><![CDATA["+Content+"]]></Content>\n"
				+ "</xml>";
		
		return str;
	}

}
