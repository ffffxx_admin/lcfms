package cn.lcfms.app.weixin.controller;

import java.io.IOException;
import java.util.Enumeration;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import cn.lcfms.app.weixin.service.WxApiService;
import cn.lcfms.app.weixin.util.WeixinDecrypMsg;
import cn.lcfms.app.weixin.util.WeixinEvent;
import cn.lcfms.app.weixin.util.WeixinMsg;
import cn.lcfms.bin.BaseService;
import cn.lcfms.bin.annotation.PermitPoll;
import cn.lcfms.bin.core.App;
import cn.lcfms.bin.view.CommonEditView;
import cn.lcfms.utils.TimeUtils;
import net.sf.json.JSONObject;

@PermitPoll(type="微信API",name="参数配置",forward="/nopermit.html")
@Controller("weixin.ConfigController")
@RequestMapping("/weixin/config")
public class ConfigController extends WeixinBaseController{
	private static Logger logger = Logger.getLogger(ConfigController.class); 
	@Autowired
	private WxApiService ws;	
	@RequestMapping("/init")
	public CommonEditView init(HttpServletRequest request) {
		CommonEditView view=new CommonEditView("微信配置");
		view.setInputForm("服务器地址(URL)", "url", request.getAttribute("APP")+"admin/weixin/notice", "readonly");
		view.setInputForm("AppId", "appid", ws.getAppid(), "");
		view.setInputForm("AppSecret", "AppSecret", ws.getAppSecret(), "");
		view.addObject("token", ws.getToken());
		view.setViewForm("weixin/token.jsp");
		view.setInputForm("EncodingAESKey", "EncodingAESKey", ws.getEncodingAESKey(), "");
		view.setInputForm("最新access_token", "access_token", ws.getAccess_token(), "");	
		view.setFormAction("save");
		view.setMethodAjax();
		return view;
	}	
	
	@RequestMapping("/save")
	public String save(HttpServletRequest request) {
		BaseService service = App.getService("weixin");
		@SuppressWarnings("rawtypes")
		Enumeration names = request.getParameterNames();
		while(names.hasMoreElements()) {
			String name = (String) names.nextElement();
			service.where("name", name).setData(request.getParameter(name)).update("value");
		}
		ws.freshConfig();
		return "{code:1,msg:保存成功！}";
	}
	
	/*
	 * 微信消息处理
	 */
	@RequestMapping("/notice")
	@PermitPoll(type="all")
	@ResponseBody
	public String notice(HttpServletRequest request,HttpServletResponse response) throws IOException{	
		@SuppressWarnings("unchecked")
		Enumeration<String> p = request.getParameterNames();
		while(p.hasMoreElements()) {
			String e =p.nextElement();
			logger.info(e+"=>"+request.getParameter(e));
		}
		ServletInputStream ris = request.getInputStream();
		if(null!=ris) {
			StringBuffer content = new StringBuffer();  
			byte[] b = new byte[1024];  
			int lens = -1;  
			while((lens = ris.read(b))>0) {  
			     content.append(new String(b, 0, lens));  
			}  
			//处理微信消息
			if(content.length()>0) {
				String str = content.toString();
				String msg_signature=request.getParameter("msg_signature");
				String timestamp=request.getParameter("timestamp");
				String nonce=request.getParameter("nonce");
				String openid=request.getParameter("openid");
				JSONObject msg = WeixinDecrypMsg.decryptMsg(ws,msg_signature, timestamp, nonce, str);				
				String MsgType = msg.getString("MsgType");				
				String myOpenId = msg.getString("ToUserName");
				WeixinMsg weixinMsg=new WeixinMsg();
				weixinMsg.setFromUserName(myOpenId);
				weixinMsg.setToUserName(openid);
				weixinMsg.setCreateTime(TimeUtils.getCurrentShortTime());
				switch (MsgType) {
				//事件消息处理
				case "event":
					String Event = msg.getString("Event");
					String EventKey = msg.getString("EventKey");									
					//点击菜单事件
					if(Event.equals("CLICK")) {						
						BaseService service=App.getService("wxmenu");
						String message = service.where("id="+EventKey).selectColumn("message");
						weixinMsg.setContent(message);
						String textMsg = weixinMsg.textMsg();
						return textMsg;
					}	
					//扫一扫事件，完毕后将结果发送给用户，url则跳转
					if(Event.equals("scancode_push")) {
						JSONObject ScanCodeInfo=msg.getJSONObject("ScanCodeInfo");
						return WeixinEvent.scancode_push(weixinMsg,EventKey,ScanCodeInfo);
					}
					//扫一扫事件，完毕后关闭照相机
					if(Event.equals("scancode_waitmsg")) {
						JSONObject ScanCodeInfo=msg.getJSONObject("ScanCodeInfo");
						return WeixinEvent.scancode_waitmsg(weixinMsg,EventKey,ScanCodeInfo);
					}
					//调用照相机事件
					if(Event.equals("pic_sysphoto")) {
						JSONObject SendPicsInfo=msg.getJSONObject("SendPicsInfo");
						return WeixinEvent.pic_sysphoto(weixinMsg,EventKey,SendPicsInfo);
					}
					//调用照相机或获取系统相册相片事件
					if(Event.equals("pic_photo_or_album")) {
						JSONObject SendPicsInfo=msg.getJSONObject("SendPicsInfo");
						return WeixinEvent.pic_photo_or_album(weixinMsg,EventKey,SendPicsInfo);
					}
					//调用微信相册事件
					if(Event.equals("pic_weixin")) {
						JSONObject SendPicsInfo=msg.getJSONObject("SendPicsInfo");
						return WeixinEvent.pic_weixin(weixinMsg,EventKey,SendPicsInfo);
					}
					//获取位置事件
					if(Event.equals("location_select")) {	
						JSONObject SendLocationInfo=msg.getJSONObject("SendLocationInfo");
						return WeixinEvent.location_select(weixinMsg,EventKey,SendLocationInfo);
					}				
				//图片消息处理	
				case "image":
					weixinMsg.setContent("图片已经收到，谢谢支持！");					
					return weixinMsg.textMsg();
				//位置消息处理
				case "location":
					weixinMsg.setContent("位置已经收到，谢谢支持！");					
					return weixinMsg.textMsg();
				default:
					break;
				}
		
			}
		}		
		String echostr=request.getParameter("echostr");
		//验证微信公众号
		if(null!=echostr) {
			return echostr;
		}		
		return "success";
	}
	
	
	
}
