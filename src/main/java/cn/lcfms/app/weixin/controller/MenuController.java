package cn.lcfms.app.weixin.controller;


import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import cn.lcfms.app.weixin.bean.WxmenuBean;
import cn.lcfms.app.weixin.service.WxApiService;
import cn.lcfms.bin.BaseService;
import cn.lcfms.bin.Notice;
import cn.lcfms.bin.annotation.PermitPoll;
import cn.lcfms.bin.core.App;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

@PermitPoll(type="微信API",name="菜单管理",forward="/nopermit.html")
@Controller("weixin.MenuController")
@RequestMapping("/weixin/menu")
public class MenuController extends WeixinBaseController{	
	@Autowired
	private WxApiService ws;
	
	@RequestMapping("/showMenu")
	public ModelAndView showMenu(@RequestParam(defaultValue="0",required=false)int id,@RequestParam(defaultValue="0",required=false)int pid) {
		ModelAndView view=new ModelAndView("admin/weixin/menu");
		BaseService service=App.getService("wxmenu");
		WxmenuBean menu=new WxmenuBean();
		if(id!=0) {
			menu=service.selectOne(WxmenuBean.class, id);
		}
		if(pid!=0) {
			String title=service.where("pid="+pid).selectColumn("title");
			menu.setPid(pid);
			menu.setPtitle(title);
		}			
		service.column("id","pid","title","type");		
		List<HashMap<String, Object>> list = service.selectList();
		HashMap<String, Object> m=new HashMap<>();
		m.put("title", "新增菜单");
		m.put("id", 0);	
		m.put("pid", 0);	
		m.put("type", "plus");	
		list.add(m);
		//添加菜单
		if(id==0) {
			m.put("active", "active");
		}else {			
			for(HashMap<String, Object> map:list) {
				int _pid=(int)map.get("pid");
				int _id=(int)map.get("id");	
				//修改一级菜单
				if(_id==menu.getId() && _pid==0) {
					map.put("active", "active");
					break;
				//修改子菜单
				}else if(_id==menu.getPid()) {
					map.put("active", "active");
					break;
				}
			}	
		}		
		view.addObject("menu", menu);	
		view.addObject("list", list);		
		return view;
	}
	
	@RequestMapping("/saveMenu")
	public String saveMenu(WxmenuBean bean) {
		BaseService service=App.getService("wxmenu");
		int[] child = bean.getChild();
		if(null!=child && child.length>5) {
			return "{code:0,msg:一个一级菜单最多分配5个子菜单！}";
		}
		if(bean.getId()==0) {
			if(bean.getPid()==0) {
				int count = service.where("pid=0").selectCount();
				if(count>=3) {
					return "{code:0,msg:主菜单最多只能添加三个！}";
				}
			}else {
				int count = service.where("pid="+bean.getPid()).selectCount();
				if(count>=5) {
					return "{code:0,msg:子菜单最多只能添加五个！}";
				}
			}				
			service.insert(bean);
			int insert_id = service.insert_id();
			if(null!=child) {
				service.whereIn("id", child).setData(insert_id).update("pid");	
			}	
			return "{code:1,msg:添加成功！,url:showMenu?id="+insert_id+"}";
		}else {
			service.update(bean);
			if(null!=child) {
				service.whereIn("id", child).setData(bean.getId()).update("pid");	
			}
			return "{code:1,msg:修改成功！,url:showMenu?id="+bean.getId()+"}";
		}		
	}
	
	@RequestMapping("/deleteMenu")
	public String deleteMenu(int id) {
		BaseService service=App.getService("wxmenu");
		int count = service.where("pid="+id).selectCount();
		if(count>0) {
			return "{code:0,msg:不能删除有子菜单的一级菜单！}";
		}
		service.deleteById(id);
		return "{code:1,msg:删除成功！,url:showMenu}";
	}
	
	@RequestMapping("/upload")
	public String upload() {
		BaseService service=App.getService("wxmenu");
		service.column("id","pid","type","url","title");
		List<HashMap<String, Object>> list = service.selectList();
		if(list.size()>0) {
			String json = listToJson(list,0,3);
			JSONObject jo=JSONObject.fromObject("{\"button\":"+json+"}");
			Notice notice = ws.setMenu(jo);
			if(notice.getCode()==0) {
				return notice.toString();
			}
		}
		return "{code:1,msg:同步成功！}";
	}
	
	@RequestMapping("/download")
	public String download() {
		return "{code:1,msg:下载成功！,url:showMenu}";
	}
	
	@RequestMapping("/clearMenu")
	public String clearMenu() {
		Notice notice = ws.deleteMenu();
		return notice.toString();
	}
	
	/**
	 * 
	 * @param list
	 * @param pid 上级id
	 * @param number 限制个数
	 * @return
	 */
	private String listToJson(List<HashMap<String, Object>> list,int pid,int number) {		
		JSONArray array=new JSONArray();		
		for(HashMap<String, Object> map:list) {
			int _pid=(int)map.get("pid");
			int id=(int)map.get("id");
			String title=(String)map.get("title");
			String type=(String)map.get("type");
			String url=(String)map.get("url");
			int level=0;
			if(_pid==pid && level<(number-1)) {
				JSONObject object=new JSONObject();
				object.put("name", title);
				if(type.equals("sub_button")) {					
					object.put("sub_button", listToJson(list,id,5));
				}else {					
					if(type.equals("view")) {
						object.put("type", "view");
						object.put("url", url);
					}
					if(type.equals("textMessage")) {
						object.put("type", "click");
						object.put("key", id);
					}
					if(type.equals("scancode_push")) {
						object.put("type", "scancode_push");
						object.put("key", id);
					}
					if(type.equals("scancode_waitmsg")) {
						object.put("type", "scancode_waitmsg");
						object.put("key", id);
					}
					if(type.equals("pic_weixin")) {
						object.put("type", "pic_weixin");
						object.put("key", id);
					}
					if(type.equals("pic_photo_or_album")) {
						object.put("type", "pic_photo_or_album");
						object.put("key", id);
					}
					if(type.equals("pic_sysphoto")) {
						object.put("type", "pic_sysphoto");
						object.put("key", id);
					}
					if(type.equals("location_select")) {
						object.put("type", "location_select");
						object.put("key", id);
					}
				}				
				array.add(object);
				level++;
			}
		}		
		return array.toString();
	}
	
}
