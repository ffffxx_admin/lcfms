package cn.lcfms.app.weixin.controller;

import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import cn.lcfms.app.weixin.service.WxApiService;
import cn.lcfms.bin.annotation.PermitPoll;
import cn.lcfms.utils.StringUtils;

@PermitPoll(type="微信API",name="素材管理",forward="/nopermit.html")
@Controller("weixin.MaterialController")
@RequestMapping("/weixin/material")
public class MaterialController extends WeixinBaseController{
	@Autowired
	private WxApiService ws;
	@RequestMapping("/list")
	public ModelAndView list(HttpServletRequest request) {
		ModelAndView view=new ModelAndView("admin/weixin/material");
		int pageNumber=1;
		int pageSize=6;
		if(null!=request.getParameter("pageNumber")){
			pageNumber=StringUtils.StringToInteger(request.getParameter("pageNumber"), 1);
		}
		if(null!=request.getParameter("pageSize")){
			pageSize=StringUtils.StringToInteger(request.getParameter("pageSize"), pageSize);
		}
		List<HashMap<String, Object>> list = ws.getMaterialList((pageNumber-1)*pageSize, pageSize);
		view.addObject("list", list);
		int count=0;
		if(list.size()>0) {
			count= (int) list.get(0).get("count");
			view.addObject("count",count);
		}
		return view;
	}
}
