package cn.lcfms.app.index.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.ModelAndView;

import cn.lcfms.app.BaseController;
public class IndexBaseController extends BaseController{
	/**
	 * 该方法将在执行控制器之前被执行
	 */
	@Override
    protected void beforeProtected(HttpServletRequest request,HttpServletResponse response){
		super.beforeProtected(request,response);
    	String basePath=(String) request.getAttribute("APP");
    	request.setAttribute("JS", basePath+"statics/index/js/"); 
    	request.setAttribute("CSS", basePath+"statics/index/css/"); 
    	request.setAttribute("IMG", basePath+"statics/index/images/");   
    }  
    /**
   	 * 该方法将在执行控制器之后，调入模板之前被执行
     * @throws IOException 
   	 */
    @Override
    protected void afterProtected(HttpServletRequest request,HttpServletResponse response,ModelAndView view) throws IOException{
    	super.afterProtected(request,response,view);   	
    }
}
