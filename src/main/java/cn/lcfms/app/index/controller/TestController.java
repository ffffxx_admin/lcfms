package cn.lcfms.app.index.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.lcfms.app.admin.bean.DemoBean;
import cn.lcfms.app.admin.service.DemoService;
import cn.lcfms.app.admin.service.Dao.DemoDao;
import cn.lcfms.app.index.service.TestService;
import cn.lcfms.app.index.service.dao.TestDao;
import cn.lcfms.bin.annotation.PermitPoll;
import cn.lcfms.utils.Vardump;
@Controller("index.TestController")
@RequestMapping("/index/test")
@PermitPoll(type="测试",name="测试service",forward="/login.html")
public class TestController extends IndexBaseController{

	@Autowired
	private DemoDao dao1;
	
	@Autowired
	private TestDao dao2;
	
	@Autowired
	private DemoService service1;
	
	@Autowired
	private TestService service2;
	
	@ResponseBody	
	@RequestMapping("/test1")
	public String test1(){
		DemoBean demo = dao1.getDemo(1);
		Vardump.print(demo);
		return "测试外层dao";
	}
	
	@ResponseBody	
	@RequestMapping("/test2")
	public String test2(){
		List<?> list = dao2.getList();
		Vardump.print(list);
		return "测试内层dao";
	}
			
	@ResponseBody	
	@RequestMapping("/test3")
	public String test3(){
		service1.getDemo();
		return "测试外层service";
	}
			
	@ResponseBody	
	@RequestMapping("/test4")
	public String test4(){
		service2.getList();
		return "测试内层service";
	}
	
	@RequestMapping("/test5")
	public String test5(HttpServletRequest request) {
		request.setAttribute("aaa", 111);
		return "abc";
	}
}
