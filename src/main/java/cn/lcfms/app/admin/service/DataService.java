package cn.lcfms.app.admin.service;

import net.sf.json.JSONObject;

import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.lcfms.app.admin.bean.ArticleBean;
import cn.lcfms.app.admin.bean.CategoryBean;
import cn.lcfms.bin.BaseService;
import cn.lcfms.bin.core.App;
@Service
public class DataService {
	@Transactional
	public boolean saveArticle(ArticleBean article){
		boolean b=true;
		try {
			BaseService service = App.getService("articles");	
			if(article.getId()==0) {
				service.insert(article);
			}else {
				HashMap<String, Object> map=new HashMap<>();
				map.put("id", article.getId());
				map.put("title", article.getTitle());
				map.put("category_id", article.getCategory_id());
				map.put("labels", article.getLabels());
				map.put("desc", article.getDesc());
				map.put("details", article.getDetails());
				map.put("mid",article.getMid());
				service.where("id=#{id}");
				service.setData(map);
				service.update("title","category_id","labels","desc","details","mid");
			}
			int id = article.getId();
			String[] uploads = article.getUploadFileName();
			if(id!=0 && null!=uploads){	
				for(int i=0;i<uploads.length;i++){
					String file = uploads[i];
					JSONObject object=JSONObject.fromObject(file);					
					if(object.get("type").equals("img")){
						int fid=(int) object.get("id");
						service.setTable("files");
						service.where("id="+fid);
						service.setData(id,"articles");
						service.update("file_type_id","file_type");
					}
				}
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return b;
	}
	
	@Transactional
	public boolean saveCategory(CategoryBean cate){
		boolean b=true;
		try {	
			BaseService service=App.getService("categories");	
			if(cate.getId()!=0) {
				HashMap<String, Object> map=new HashMap<>();
				map.put("id", cate.getId());
				map.put("title", cate.getTitle());
				map.put("topid", cate.getTopid());
				map.put("mid",cate.getMid());
				map.put("is_top_showing", cate.getIs_top_showing());
				map.put("desc", cate.getDesc());
				service.where("id=#{id}");
				service.setData(map);
				service.update("title","topid","is_top_showing","mid","desc");			
			}else {
				service.insert(cate);
			}		
			int id = cate.getId();
			String[] uploads = cate.getUploadFileName();
			if(id!=0 && null!=uploads){			
				for(int i=0;i<uploads.length;i++){
					String file = uploads[i];
					JSONObject object=JSONObject.fromObject(file);		
					if(object.get("type").equals("img")){
						int fid=(int) object.get("id");
						service.setTable("files");
						service.where("id="+fid);
						service.setData("categories",id);
						service.update("file_type","file_type_id");
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			b=false;
		}
		return b;
	}
	
	/**
	 * 根据id递归获取栏目
	 * @param id
	 * @return
	 */
	public HashMap<Integer, String> getCategory(int id){
		BaseService service=App.getService("categories");
		List<HashMap<String, Object>> list = service.selectList();
		HashMap<Integer, String> map=new HashMap<>();
		getCategory(id,list,map);
		return map;
	}
	
	private void getCategory(int id,List<HashMap<String, Object>> list,HashMap<Integer, String> map) {
		for(int i=0;i<list.size();i++) {
			int cid=(int) list.get(i).get("id");
			int topid=(int) list.get(i).get("topid");
			String title=(String) list.get(i).get("title");
			if(cid==id) {
				map.put(cid, title);
				if(topid!=0) {
					getCategory(topid,list,map);
				}
			}
		}
	}
	
	public String[] getModel() {
		BaseService service=App.getService("model");
		service.column("mid","name");
		List<HashMap<String, Object>> list = service.selectList();
		StringBuffer text=new StringBuffer();
		StringBuffer value=new StringBuffer();
		for(int i=0;i<list.size();i++) {
			HashMap<String, Object> map=list.get(i);
			text.append(map.get("name"));
			value.append(map.get("mid"));
			if(i!=list.size()-1) {
				text.append(",");
				value.append(",");
			}
		}
		String[] r= {text.toString(),value.toString()};
		return r;
	}
}
