package cn.lcfms.app.admin.controller;


import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import cn.lcfms.app.admin.bean.TagBean;
import cn.lcfms.bin.BaseService;
import cn.lcfms.bin.annotation.PermitPoll;
import cn.lcfms.bin.core.App;


@PermitPoll(type="系统功能",name="标签管理",forward="/nopermit.html")
@Controller("admin.TagController")
@RequestMapping("/admin/tag")
public class TagController extends AdminBaseController{
	/*
	 * 管理标签
	 */
	@RequestMapping("/manage")
	public ModelAndView manage(){
		BaseService service=App.getService("tag");
		List<HashMap<String, Object>> list = service.selectList();
		ModelAndView view=new ModelAndView("admin/frame/tagManage","list",list);
		return view;
	}
	/*
	 * 增加标签页面
	 */
	@RequestMapping("/addview")
	public ModelAndView addview(TagBean bean){
		BaseService service=App.getService("tag");
		ModelAndView view=new ModelAndView("admin/frame/tagAdd");
		List<HashMap<String, Object>> list = service.where("parentId=0").selectList();
		view.addObject("tagType", list);
		if(bean.getTagId()!=0){
			bean=service.selectOne(TagBean.class, bean.getTagId());
		}
		view.addObject("tag", bean);
		return view;
	}
	/*
	 * 处理增加或修改标签
	 */
	@RequestMapping("/add")
	public String add(TagBean bean){
		BaseService service=App.getService("tag");
		if(bean.getTagId()==0){
			service.insert(bean);
		}else{
			service.update(bean);
		}	
		return "local:manage";
	}
	/*
	 * 删除标签
	 */
	@RequestMapping("/delete")
	public String delete(int tagId){
		BaseService service=App.getService("tag");
		service.deleteById(tagId);
		return "local:manage";
	}
	
	public static void main(String[] args) {
		String str="a.m=123 and t12.s=12 and m.cc=#{dd}";
		String regex="\\w+=";
		Pattern pattern=Pattern.compile(regex);
		Matcher matcher = pattern.matcher(str);
		while(matcher.find()){
			String p=matcher.group();
			str=str.replaceFirst(p, "`"+p.substring(0, p.length()-1)+"`=");
		}
		System.out.println(str);
	}
}
