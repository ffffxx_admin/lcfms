package cn.lcfms.app.admin.controller;

import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import net.sf.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import cn.lcfms.app.admin.bean.FilesBean;
import cn.lcfms.bin.BaseService;
import cn.lcfms.bin.annotation.PermitPoll;
import cn.lcfms.bin.core.App;
import cn.lcfms.bin.view.CommonEditView;
import cn.lcfms.bin.view.CommonPageView;

@PermitPoll(type="CMS管理",name="资料管理",forward="/nopermit.html")
@Controller("admin.DataController")
@RequestMapping("/admin/data")
public class DataController extends AdminBaseController{
	
	@RequestMapping("pictureAdd")
	public CommonEditView pictureAdd(HttpServletRequest request){		
		CommonEditView cev=new CommonEditView("上传图片");
		cev.setInputForm("图片名称", "name", "", "unempty");
		cev.setSelectForm("关联主题", "file_type", "文章类型,栏目类型", "articles,categories", "article");		
		cev.setInputForm("关联id", "file_type_id", "","unempty +integer");
		cev.setUploadImageForm("图片文件");
		cev.setViewForm("frame/checkFileType.jsp");
		cev.setFormAction("savePicture");
		return cev;
	}
	
	@RequestMapping("checkFileType")
	@ResponseBody
	public String checkFileType(int id,String table) {
		BaseService service = App.getService(table);
		String primaryKey = service.getPrimaryKey();
		int count = service.where(primaryKey+"="+id).selectCount();
		if(count==0) {
			return "0";
		}
		return "表"+table+"中存在该主题";
	}
	
	@RequestMapping("pictureList")
	public CommonPageView pictureList(HttpServletRequest request){
		BaseService service=App.getService("files a");
		service.column("a.id as id");
		service.column("a.name as 名称");	
		service.column("a.file_type as 主题类型",(Object object)->{
			String str=(String)object;					
			if(str.equals("articles")){
				return "文章类型";
			}
			if(str.equals("categories")){
				return "栏目类型";
			}
			return "";
		});	
		service.column("a.file_type_id as 关联ID");	
		service.column("CONCAT(a.id,',',a.name,',',a.original,',',a.thumbnail) as 图片预览",(Object object)->{
			String string=(String)object;
			String[] split = string.split(",");
			String basePath=(String) request.getAttribute("APP");
			String result="<img style=\"cursor:pointer;\" alt=\""+split[1]+"\" id=\""+split[0]+"\" original=\""+basePath+"uploadfile/images/"+split[2]+"\" width=\"100px\" src=\""+basePath+"uploadfile/images/"+split[3]+"\"/>";
			return result;
		});	
		service.where("a.type='img'");
		service.order("a.id desc");
		CommonPageView cpv=new CommonPageView("图片资料",service);		
		cpv.setFilterInputLike("图片名称", "a.title", "");
		cpv.setPageSize(5);
		cpv.setUpdateUrl("editPic?pid=");
		cpv.setDeleteUrl("deletePic?pid=");
		return cpv;
	}	
	
	@RequestMapping("savePicture")
	public String savePicture(FilesBean files){
		BaseService service=App.getService("files");
		String[] file = files.getUploadFileName();
		if(null!=file && file.length>0) {
			for(int i=0;i<file.length;i++){
				String f = file[i];
				JSONObject object=JSONObject.fromObject(f);	
				if(object.get("type").equals("img")){
					int id=(int) object.get("id");
					files.setId(id);
					service.update(files,"thumbnail,original");
				}
			}	
		}else {
			service.update(files,"thumbnail,original");
		}		
		return "local:pictureList";
	}
	
	@RequestMapping("deletePic")
	public String deletePic(int pid){
		BaseService service=App.getService("files");
		service.deleteById(pid);
		return "local:pictureList";
	}
	
	@RequestMapping("editPic")
	public CommonEditView editPic(int pid){
		BaseService service=App.getService("files");
		service.where("id=#{id}");
		service.setData(pid);
		HashMap<String, Object> map = service.selectMap();		
		CommonEditView cev=new CommonEditView("修改图片资料");
		cev.setInputForm("图片名称", "name", map.get("name"), "unempty");
		cev.setSelectForm("关联主题", "file_type", "文章类型,栏目类型", "articles,categories", map.get("file_type"));		
		cev.setInputForm("关联id", "file_type_id", map.get("file_type_id"),"unempty +integer");
		cev.setViewForm("frame/checkFileType.jsp");
		cev.setHiddenForm("id", pid);
		cev.setFormAction("savePicture");
		return cev;
	}
	
}
