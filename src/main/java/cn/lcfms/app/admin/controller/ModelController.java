package cn.lcfms.app.admin.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import cn.lcfms.app.admin.bean.ModelBean;
import cn.lcfms.bin.BaseService;
import cn.lcfms.bin.annotation.PermitPoll;
import cn.lcfms.bin.core.App;
import cn.lcfms.bin.view.CommonPageView;
import cn.lcfms.bin.view.CommonEditLayer;

@PermitPoll(type="CMS管理",name="文章管理",forward="/nopermit.html")
@Controller("admin.ModelController")
@RequestMapping("/admin/model")
public class ModelController extends AdminBaseController{
	
	@RequestMapping("/index")
	public CommonPageView index() {
		BaseService service=App.getService("model");
		service.column("mid","mid as id","name as 名称","description as 描述","template as 模板","data as 数据");	
		service.order("mid");
		CommonPageView view=new CommonPageView("模型列表", service);
		view.setToolBarLayer("添加模型", "fa-plus", "/admin/model/plus", 500, 375);
		view.setDeleteLayer("delete?mid=");
		view.setUpdateLayer("update?mid=", 500, 375);
		view.setColumnWidth("mid", 0);
		return view;
	}
	
	@RequestMapping("/plus")
	public CommonEditLayer plus() {
		CommonEditLayer layer=new CommonEditLayer();
		layer.setInputForm("标题", "name", "", "unempty");
		layer.setInputForm("模板", "template", "", "unempty");
		layer.setInputForm("数据", "data", "", "");
		layer.setTextareaForm("描述", "description", "", 200);
		layer.setFormAction("save");
		return layer;
	}
	
	@RequestMapping("/update")
	public CommonEditLayer update(int mid) {
		CommonEditLayer layer=new CommonEditLayer();
		BaseService service = App.getService("model");
		ModelBean bean=service.selectOne(ModelBean.class, mid);	
		layer.setInputForm("标题", "name", bean.getName(), "unempty");
		layer.setInputForm("模板", "template", bean.getTemplate(), "unempty");
		layer.setInputForm("数据", "data", bean.getData(), "");
		layer.setTextareaForm("描述", "description", bean.getDescription(), 200);
		layer.setHiddenForm("mid", bean.getMid());
		layer.setFormAction("save");
		return layer;
	}
	
	@RequestMapping("/save")
	public String save(ModelBean model) {
		if(model.getMid()==0) {
			App.getService("model").insert(model);
			return "{code:1,msg:添加成功,url:index}";
		}else {
			App.getService("model").update(model);
			return "{code:1,msg:修改成功,url:index}";
		}		
	}
	
	@RequestMapping("/delete")
	public String delete(int mid) {
		App.getService("model").deleteById(mid);
		return "local:index";
	}
	
}
