package cn.lcfms.app.admin.bean;

public class AreasBean {
	private int s_province;
	private int s_city;
	private int s_county;
	private int s_street;
	private String s_province_str;
	private String s_city_str;
	private String s_county_str;
	private String s_street_str;
	
	public AreasBean() {
		
	}
	public AreasBean(int s_province) {
		this.setS_province(s_province);
	}
	public AreasBean(int s_province,int s_city) {
		this.setS_province(s_province);
		this.setS_city(s_city);
	}
	public AreasBean(int s_province,int s_city,int s_county) {
		this.setS_province(s_province);
		this.setS_city(s_city);
		this.setS_county(s_county);
	}
	public AreasBean(int s_province,int s_city,int s_county,int s_street) {
		this.setS_province(s_province);
		this.setS_city(s_city);
		this.setS_county(s_county);
		this.setS_street(s_street);
	}

	@Override
	public String toString(){
		String rString=this.s_province_str+","+s_city_str+","+s_county_str+","+s_street_str;		
		return rString.replaceAll(",null", "");
	}
	public int getS_province() {
		return s_province;
	}
	public void setS_province(int s_province) {
		this.s_province = s_province;
	}
	public int getS_city() {
		return s_city;
	}
	public void setS_city(int s_city) {
		this.s_city = s_city;
	}
	public int getS_county() {
		return s_county;
	}
	public void setS_county(int s_county) {
		this.s_county = s_county;
	}
	public int getS_street() {
		return s_street;
	}
	public void setS_street(int s_street) {
		this.s_street = s_street;
	}
	public String getS_province_str() {
		return s_province_str;
	}
	public void setS_province_str(String s_province_str) {
		this.s_province_str = s_province_str;
	}
	public String getS_city_str() {
		return s_city_str;
	}
	public void setS_city_str(String s_city_str) {
		this.s_city_str = s_city_str;
	}
	public String getS_county_str() {
		return s_county_str;
	}
	public void setS_county_str(String s_county_str) {
		this.s_county_str = s_county_str;
	}
	public String getS_street_str() {
		return s_street_str;
	}
	public void setS_street_str(String s_street_str) {
		this.s_street_str = s_street_str;
	}
	
}
