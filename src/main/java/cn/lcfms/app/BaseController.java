package cn.lcfms.app;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;

import cn.lcfms.bin.BaseService;
import cn.lcfms.bin.core.App;
import cn.lcfms.cms.Index;
public class BaseController {
	/**
	 * 该方法将在执行控制器之前被执行
	 */
    protected void beforeProtected(HttpServletRequest request,HttpServletResponse response){   	 	
    	String contextPath = request.getContextPath();
	    String basePath = "";
	    if(request.getServerPort()==80){
	    	basePath = request.getScheme()+"://"+request.getServerName()+contextPath+"/";
	    }else{
	    	basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+contextPath+"/";
	    }	
	    request.setAttribute("APP", basePath); 		    
    }  
    /**
   	 * 该方法将在执行控制器之后，调入模板之前被执行
     * @throws IOException 
   	 */
    protected void afterProtected(HttpServletRequest request,HttpServletResponse response,ModelAndView view) throws IOException{  	    	
    	if(view!=null && view.getViewName()!=null){	
    		String viewname=view.getViewName();	 
			if(viewname.startsWith("[") && viewname.endsWith("]")){
				response.setContentType("application/json;charset=utf-8");
				PrintWriter writer = response.getWriter();
	    		writer.write(viewname);
	    		writer.flush();
	    		writer.close();
	    		view.clear();
			}
			if(viewname.startsWith("{") && viewname.endsWith("}")){
				response.setContentType("application/json;charset=utf-8");
				PrintWriter writer = response.getWriter();			
				if(viewname.indexOf("\"")==-1) {
					StringBuffer buffer=new StringBuffer("{");
					viewname=viewname.substring(1, viewname.length()-1);
					String[] split = viewname.split(",");
					for(int i=0;i<split.length;i++) {
						String str=split[i];
						int index=str.indexOf(":");
						int length=str.length();
						buffer.append("\""+str.substring(0, index)+"\":\""+str.substring(index+1, length)+"\"");
						if(i!=split.length-1) {
							buffer.append(",");
						}			
					}	
					buffer.append("}");
					viewname=buffer.toString();
				}
				writer.write(viewname);
	    		writer.flush();
	    		writer.close();
	    		view.clear();
			}
		}
    }
	
	protected ModelAndView Cms(String table,int id,HttpServletRequest request,HttpServletResponse response) {
		try {
			BaseService service = App.getService(table);
			service.leftJoin("model").on(table+".mid=model.mid");
			service.where(table+".id=#{id}");
			service.setData(id);
			service.column("model.template","model.data");
			HashMap<String, Object> map = service.selectMap();
			String temp=(String) map.get("template");
			temp=temp.substring(0, temp.indexOf(".jsp"));
			ModelAndView view=new ModelAndView(temp);
			request.setAttribute("CURRENTId", id);
			String datas=(String) map.get("data");
			if(null!=datas && !"".equals(datas)) {
				String[] dataarr =datas.split(",");
				Index data=new Index(request,response,id,service);
				for(String d:dataarr) {
					try {
						Method method = data.getClass().getMethod(d);
						method.invoke(data);				
					} catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
						e.printStackTrace();
					}
				}
			}		
			return view;
		} catch (Exception e) {
			ModelAndView view=new ModelAndView("404");
			return view;
		}
	}
}
