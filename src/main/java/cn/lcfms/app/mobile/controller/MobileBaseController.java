package cn.lcfms.app.mobile.controller;

import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.web.servlet.ModelAndView;
import cn.lcfms.app.BaseController;
public class MobileBaseController extends BaseController{
	/**
	 * 该方法将在执行控制器之前被执行
	 */
	@Override
    protected void beforeProtected(HttpServletRequest request,HttpServletResponse response){
		super.beforeProtected(request,response);
    	String basePath=(String) request.getAttribute("APP");
    	request.setAttribute("APP", basePath); 
    	request.setAttribute("JS", basePath+"statics/mobile/js/"); 
    	request.setAttribute("CSS", basePath+"statics/mobile/css/"); 
    	request.setAttribute("IMG", basePath+"statics/mobile/images/");   
    }  
    /**
   	 * 该方法将在执行控制器之后，调入模板之前被执行
     * @throws IOException 
   	 */
    @Override
    protected void afterProtected(HttpServletRequest request,HttpServletResponse response,ModelAndView view) throws IOException{
    	super.afterProtected(request,response,view);   
    	if(view!=null && view.getViewName()!=null){	
    		String viewname=view.getViewName();	 
    		request.setAttribute("TEMPLATE", viewname+".jsp");
    		view.setViewName("mobile/common");
    	}
    }
}
