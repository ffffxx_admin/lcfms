<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript" src="${JS}tab.js"></script>
<script type="text/javascript" src="${JS}index.js"></script>
<script type="text/javascript" src="${JS}jquery.SuperSlide.2.1.1.js"></script>
<!--轮播图-->
<div class="swiper-container swiper-container1">
	<div class="swiper-wrapper">
		<div class="swiper-slide"><a target="_self" href="javascript:void(0)"><img src="${IMG}bann2.jpg" width="100%"> </a></div>
		<div class="swiper-slide"><a target="_self" href="javascript:void(0)"><img src="${IMG}bann4.jpg" width="100%" > </a></div>
	</div>
	<!--分页-->
	<div style="bottom:2px;" class="swiper-pagination"></div>
	<!-- 分页背景-->
	<div class="fy_bg"></div>
</div>
 <div class="main4">
	 <div class="common_nav common_title3">
		<a target="_self" href="javascript:void(0);"><img src="${IMG}main5_nav.jpg" width="100%"/></a>
	 </div>
	 <div class="picScroll-left">
			<div class="bd">
				<ul class="picList">
				    <c:forEach items="${kfal}" var="kf">
					<li>
						<div class="pic"><a href="javascript:changeVideo('${kf.labels}');" target="_blank"> <img src="${APP}uploadfile/video/${kf.labels}.png" alt="${kf.title}"/></a></div>
						<div class="title"><a href="javascript:changeVideo('${kf.labels}');" target="_blank">${kf.title}..</a></div>
						<div class="jtou"><img src="${IMG}shiping.png" onclick="changeVideo('${kf.labels}');"/></div>
					</li>	
					</c:forEach>									
				</ul>
			</div>
		</div>
		<div class="anli" style="background:url(${APP}uploadfile/video/2.png) 100% no-repeat;">
		 	<video id="KFSP" controls="controls" autoplay="autoplay" width="100%"></video>
		</div>	
		<script type="text/javascript">
		jQuery(".picScroll-left").slide({mainCell:".bd ul",autoPage:true,effect:"left",autoPlay:true,vis:3,trigger:"click"});
		function changeVideo(id){
			var kf=document.getElementById("KFSP");
			//kf.src="${APP}uploadfile/video/"+id+".mp4";
			kf.src="http://gzhly.lcfms.cn/uploadfile/video/"+id+".mp4";
			kf.autoplay="autoplay";
		}
		$(function(){
			changeVideo('2');
		});
		</script> 
 </div>
<!-- main1 -->
<div class="main1">
	<div class="common">
		<div class="swiper-nav swiper-container common" id="swiper-nav1">
			<div class="swiper-wrapper" id="tab">
				<div class="swiper-slide active"><span>健康知识</span></div>
				<div class="swiper-slide"><span>医院动态</span></div>
				<div class="swiper-slide"><span>媒体聚焦</span></div>
			</div>
		</div>
		<div class="tab-bg">
			<a target="_self" href="javascript:void(0)" target="_blank"></a>
			<img src="${IMG}weichuang_tu.jpg" alt="weichuang_tu.jpg"/>
		</div>
		<div class="swiper-pages swiper-container" id="swiper-pages1">
			<div class="swiper-wrapper">
				<div class="swiper-slide">
					<div>
						<div>
							<div class="slide_cont_detail">
								<dl>
								<c:forEach items="${jkzs}" var="jk">
								<dd><a target="_self" href="/mobile/index/article/${jk.id}">【${jk.labels}】${jk.title}</a><a target="_self" style="color:#2b6f26;float:right;" href="/mobile/index/article?id=${jk.id}">【+点击咨询】</a></dd>
								</c:forEach>								
								</dl>
							</div>
						</div>
					</div>
				</div>
				<div class="swiper-slide">
					<div>
						<div>
							<div class="slide_cont_detail">
								<dl>
								<c:forEach items="${yydt}" var="jk">
								<dd><a target="_self" href="/mobile/index/article/${jk.id}">【${jk.labels}】${jk.title}</a><a target="_self" style="color:#2b6f26;float:right;" href="/mobile/index/article?id=${jk.id}">【+点击咨询】</a></dd>
								</c:forEach>
								</dl>
							</div>
						</div>
					</div>
				</div>
				<div class="swiper-slide">
					<div>
						<div>
							<div class="slide_cont_detail">
								<dl>
								<c:forEach items="${mtjj}" var="jk">
								<dd><a target="_self" href="/mobile/index/article/${jk.id}">【${jk.labels}】${jk.title}</a><a target="_self" style="color:#2b6f26;float:right;" href="/mobile/index/article?id=${jk.id}">【+点击咨询】</a></dd>
								</c:forEach>
								</dl>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="swt">
	<a target="_self" href="javascript:void(0)" style="height: 2.32rem; display: block;"><img src="${IMG}activity_2.jpg" width="100%" alt=""></a>
</div>
<!-- main2 -->
<div class="main2">
	<div class="common_nav">
		<a target="_self" style="line-height:0.5;" href="javascript:void(0);"><img src="${IMG}main2_nav.jpg" width="100%"/></a>
	</div>
	 <div class="swiper-container swiper-container1 bz-container" style="height:auto;margin:3px 0 0;padding-bottom:0.3rem;">
		 <div class="swiper-wrapper">
			 <div class="swiper-slide con_all2">
				 <ul class="subnav_bg subnav_bg1 mt_25">					
					<li class="li1"><a target="_self" href="/mobile/index/search/肺癌"><i><img src="${IMG}sy_con1_pic01.png"></i>肺癌</a></li>					
					<li class="li1"><a target="_self" href="/mobile/index/search/胃癌"><i><img src="${IMG}sy_con1_pic02.png"></i>胃癌</a></li>				
					<li class="li1"><a target="_self" href="/mobile/index/search/肝癌"><i><img src="${IMG}sy_con1_pic03.png"></i>肝癌</a></li>			
					<li class="li1"><a target="_self" href="/mobile/index/search/胰腺癌"><i><img src="${IMG}sy_con1_pic04.png"></i>胰腺癌</a></li>					
					<div class="clear"></div>
				 </ul>
				  <ul class="subnav_bg subnav_bg2 mt_25">					
					<li class="li1"><a target="_self" href="/mobile/index/search/食道癌"><i><img src="${IMG}sy_con1_pic04.png"></i>食道癌</a></li>					
					<li class="li1"><a target="_self" href="/mobile/index/search/直肠癌"><i><img src="${IMG}sy_con1_pic05.png"></i>直肠癌</a></li>					
					<li class="li1"><a target="_self" href="/mobile/index/search/胆囊癌"><i><img src="${IMG}sy_con1_pic06.png"></i>胆囊癌</a></li>				
					<li class="li1"><a target="_self" href="/mobile/index/search/淋巴癌"><i><img src="${IMG}sy_con1_pic07.png"></i>淋巴癌</a></li>					
					<div class="clear"></div>
				 </ul>
				  <ul class="subnav_bg subnav_bg3 mt_25">					
					<li class="li1"><a target="_self" href="/mobile/index/search/卵巢癌"><i><img src="${IMG}sy_con1_pic08.png"></i>卵巢癌</a></li>					
					<li class="li1"><a target="_self" href="/mobile/index/search/子宫癌"><i><img src="${IMG}sy_con1_pic09.png"></i>子宫癌</a></li>					
					<li class="li1"><a target="_self" href="/mobile/index/search/宫颈癌"><i><img src="${IMG}sy_con1_pic010.png"></i>宫颈癌</a></li>					
					<li class="li1"><a target="_self" href="/mobile/index/search/睾丸癌"><i><img src="${IMG}sy_con1_pic011.png"></i>睾丸癌</a></li>				
					<div class="clear"></div>
				 </ul>
			 </div>
			 <div class="swiper-slide con_all2">
				 <ul class="subnav_bg subnav_bg1 mt_25">				
					<li class="li1"><a target="_self" href="/mobile/index/search/鼻咽癌"><i><img src="${IMG}sy_con1_pic12.png"></i>鼻咽癌</a></li>				
					<li class="li1"><a target="_self" href="/mobile/index/search/喉癌"><i><img src="${IMG}sy_con1_pic13.png"></i>喉癌</a></li>					
					<li class="li1"><a target="_self" href="/mobile/index/search/口腔癌"><i><img src="${IMG}sy_con1_pic14.png"></i>口腔癌</a></li>					
					<li class="li1"><a target="_self" href="/mobile/index/search/膀胱癌"><i><img src="${IMG}sy_con1_pic15.png"></i>膀胱癌</a></li>				
					<div class="clear"></div>
				 </ul>
				  <ul class="subnav_bg subnav_bg2 mt_25">				
					<li class="li1"><a target="_self" href="/mobile/index/search/肾癌"><i><img src="${IMG}sy_con1_pic16.png"></i>肾癌</a></li>					
					<li class="li1"><a target="_self" href="/mobile/index/search/前列腺癌"><i><img src="${IMG}sy_con1_pic17.png"></i>前列腺癌</a></li>				
					<li class="li1"><a target="_self" href="/mobile/index/search/阴茎癌"><i><img src="${IMG}sy_con1_pic18.png"></i>阴茎癌</a></li>			
					<li class="li1"><a target="_self" href="/mobile/index/search/乳腺癌"><i><img src="${IMG}sy_con1_pic19.png"></i>乳腺癌</a></li>					
					<div class="clear"></div>
				 </ul>
				  <ul class="subnav_bg subnav_bg3 mt_25">					
					<li class="li1"><a target="_self" href="/mobile/index/search/骨癌"><i><img src="${IMG}sy_con1_pic20.png"></i>骨癌</a></li>				
					<li class="li1"><a target="_self" href="/mobile/index/search/脑瘤"><i><img src="${IMG}sy_con1_pic21.png"></i>脑瘤</a></li>					
					<li class="li1"><a target="_self" href="/mobile/index/search/甲状腺癌"><i><img src="${IMG}sy_con1_pic22.png"></i>甲状腺癌</a></li>					
					<li class="li1"><a target="_self" href="/mobile/index/search/疑难杂瘤"><i><img src="${IMG}sy_con1_pic23.png"></i>疑难杂瘤</a></li>				
					<div class="clear"></div>
				 </ul>
			 </div>
		 </div>
		 <!--分页-->
		 <div style="bottom:1px;" class="swiper-pagination"></div>
		 <!-- 分页背景-->
		 <div class="fy_bg2"></div>
	</div>
</div>
<!---专家团队--start-->
<!-- main3 --><style>.main3_cont .swiper-nav {height: 0.52rem;}</style>
<div class="main3">
	<div class="common_nav common_title1">
		<img src="${IMG}main3_nav.jpg" width="100%"/>
	</div>
	<div class="main3_cont common">
		<div class="swiper-nav swiper-container">
			<div class="swiper-wrapper" id="main3_tit">
				<div class="swiper-slide active"><span>中医传承专家</span></div>
			</div>
		</div>
		<div class="main3_detail">
			<ul id="main3_ul">
				<li>
					<div class="swiper-container swiper-container3">
						<div class="swiper-wrapper">
							<div class="swiper-slide" style="background: url(${IMG}ys-ccm.jpg) no-repeat;background-size: 6.1rem 3.31rem;">
								<div class="slide_cont_detail">
									<h2>曹昌明<span>医院院长</span></h2>
									<p>临床经验：近50年</p>
									<p>曹昌明，遵义市民间知名中医大夫，临床从事中医工作五十余年<a target="_self" style="color:#2b6f26;" href="${APP}mobile/index/article/16">【详情】</a></p>
									<div class="main3_opera">
										<a target="_self" href="javascript:shangqiao();">预约挂号</a>
										<a target="_self" href="javascript:shangqiao();">点击咨询</a>
									</div>
								</div>
							</div>
							<div class="swiper-slide" style="background: url(${IMG}ys-zyx.jpg) no-repeat;background-size: 6.1rem 3.31rem;">
								<div class="slide_cont_detail">
									<h2>张艳旭<span>肿瘤科主任</span></h2>
									<p>临床经验：近30年</p>
									<p>张艳旭，家族传承中医（妹妹），曾为国务院副总理侄女治愈乳腺癌<a target="_self" style="color:#2b6f26;" href="${APP}mobile/index/article/14">【详情】</a></p>
									<div class="main3_opera">
										<a target="_self" href="javascript:shangqiao();">预约挂号</a>
										<a target="_self" href="javascript:shangqiao();">点击咨询</a>
									</div>
								</div>
							</div>
							<div class="swiper-slide" style="background: url(${IMG}ys-zxl.jpg) no-repeat;background-size: 6.1rem 3.31rem;">
								<div class="slide_cont_detail">
									<h2>张旭丽<span>肿瘤科大夫</span></h2>
									<p>临床经验：近30年</p>
									<p>张艳旭，家族传承中医（姐姐），使用内病外治渗透疗法（魔疗）治愈<a target="_self" style="color:#2b6f26;" href="${APP}mobile/index/article/15">【详情】</a></p>
									<div class="main3_opera">
										<a target="_self" href="javascript:shangqiao();">预约挂号</a>
										<a target="_self" href="javascript:shangqiao();">点击咨询</a>
									</div>
								</div>
							</div>

						</div>
						<!--分页-->
						<div class="swiper-pagination pagination2 expert-pagination"></div>
					</div>
				</li>
			</ul>
		</div>
	</div>
</div>

<!--add疗法-->
<div class="main3" style="position:relative;">
	<div class="common_nav common_title2">
		<img src="${IMG}main4_nav.jpg" width="100%"/>
	</div>
	<div style="border-bottom:1px dashed #ccc;position:absolute;text-align:justify;text-indent: 2em;font-size: 0.27rem;line-height: 1.7;padding:21px 3% 10px;">贵遵护理医院在癌症研究领域不断探索与发现，始终秉承中医辨病辩证精髓的中医理念，大量吸取现代抗癌研究成果，提出扶正祛邪·抗癌消瘤体系…<a target="_self" href="/texiaoliaofa/13.html" style="color:#C00000;font-size:0.27rem;">[详情]</a></div>
	<a target="_self" href="/texiaoliaofa/13.html" style="display: block;"><img width="100%" src="${IMG}lftp.jpg" alt=""/></a>	
</div>  
<!--main3-->
<div class="share">
	<div class="common_nav">
		<img src="${IMG}share_nav.jpg" width="100%"/>
	</div>
	<div class="share_cont">
		<div class="share_tit">
			<div id="demo" style="overflow:hidden;">
				<div class="wyple" id="evalute">
					<dl style="margin-top: 0"><dt><img src="${IMG}tx02.gif">经典爱情日说：</dt>
						<dd>这家医院有很多看起资历很老的老中医坐诊 ，环境干净整洁，工作人员服务态度好，大夫看病仔细认真，是一家比较放心的医院</dd>
					</dl>
					<dl><dt><img src="${IMG}tx01.gif">薇薇笑：</dt>
						<dd>2年前被查出胃癌，在医院做了切除手术，后面一直靠药物控制病情，也不怎么见好。后来来到御生堂治疗，专家给他开了一个疗程的中药，吃了几天就看见了一些效果，胃口也比以前好了。一个疗程吃完后，胃痛、胃胀气的现象几乎没怎么出现过了。</dd>
					</dl>
					<dl><dt><img src="${IMG}tx04.gif">善良的村民：</dt>
						<dd>医院环境相比其它医院来说好很多，医生很有耐心，治疗之前咨询了很多问题，都很有耐心的回答，而且医生还问了很多关于我疾病的问题，说了解多一些更容易针对治疗，我感觉这个医院还不错。</dd>
					</dl>
					<dl><dt><img src="${IMG}tx03.gif">二味爷：</dt>
						<dd>我是一个右肺未分化癌患者，在这里经过治疗后，几天咳嗽、胸痛就减轻了，血丝也变少了。一个疗程结束后去医院检查，显示病灶阴影消失，真的是太开心了，对御生堂的感谢之情难以言表。
						</dd>
					</dl>
					<dl><dt><img src="${IMG}tx06.gif">英雄：</dt>
						<dd>医生态度很好，很负责任。网络预约是比排队好一点，去了直接看，很不错，最重要的是医生的医技很好，治疗了一周现在看来效果还不错。
						</dd>
					</dl>
					<dl><dt><img src="${IMG}tx03.gif">安樂書生：</dt>
						<dd>得了胃癌有1年多了，一直没找到合适的方法治疗，试过很多方法，效果都不明显，辗转来到中医肿瘤，现在治疗了2个疗程，感觉好多了，这次终于找到了合适的治疗方法了。</dd>
					</dl>
					<dl><dt><img src="${IMG}tx02.gif">缘事析理：</dt>
						<dd>朋友推荐的，说他父亲胃癌在那里治疗了的，效果很好，就带着岳母过去看看了，没想到确实如他所说的，治疗效果很好，现在岳母不怎么痛了，也能吃的下饭睡的着觉了，所以在这里也推荐给其他肿瘤朋友!</dd>
					</dl>
					<dl><dt><img src="${IMG}tx01.gif">我等你一辈子：</dt>
						<dd>父亲肺癌已经吃了张医生开的一个疗程的药，感觉效果还是很好的，就是排队时间太长了，网上挂号了我都排了半个小时，人一多心情就比较烦躁，希望医院可以改进下吧。</dd>
					</dl>
				</div>
			</div>
		</div>
	</div>
</div>
<!---评论--end-->