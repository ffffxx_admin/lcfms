<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="clear"></div>
<div class="article_weizhi">
  <div class="article_weizhi_out jz">
   	<p>
    <a href="${APP}">主页</a>
    &gt; <a target="_self" href="/mobile/index/category/19">康复案例</a> &gt;  
    </p>
   </div>
</div>
<div class="anli" style="background:url(${APP}uploadfile/video/2.png) 100% no-repeat;">
 	<video id="KFSP" controls="controls" autoplay="autoplay" width="100%"></video>
</div>
<div class="list jz">
 	<div class="picScroll-al">
	<div class="bd">
	  <ul class="picList">
	     <c:forEach items="${list}" var="kf">
	      <li>
			<div class="pic"><a href="javascript:changeVideo('${kf.labels}');" target="_blank"> <img src="${APP}uploadfile/video/${kf.labels}.png" alt="${kf.title}"/></a></div>
			<div class="title"><a href="javascript:changeVideo('${kf.labels}');" target="_blank">${kf.title}..</a></div>
			<div class="jtou"><img src="${IMG}shiping.png" onclick="changeVideo('${kf.labels}');"/></div>
		 </li>
	     </c:forEach>
	  </ul>
  </div>
  </div>
</div>
<script type="text/javascript">
function changeVideo(id){
	var kf=document.getElementById("KFSP");
	kf.src="${APP}uploadfile/video/"+id+".mp4";
	kf.autoplay="autoplay";
}
$(function(){
	changeVideo('2');
});
</script> 