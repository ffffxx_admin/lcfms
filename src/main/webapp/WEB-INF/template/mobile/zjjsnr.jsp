<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="clear"></div>
<div class="article_weizhi">
  <div class="article_weizhi_out jz">
    <p>
    <a href="${APP}">主页</a>
    <c:if test="${null!=menu_title_0}">
    &gt; <a href="/mobile/index/category?id=${menu_title_0.id}">${menu_title_0.title}</a>
    </c:if>
    <c:if test="${null!=menu_title_1}">
    &gt; <a href="/mobile/index/category?id=${menu_title_1.id}">${menu_title_1.title}</a> 
    </c:if>
    &gt;  
    </p>
  </div>
</div>
<div class="article">
   <div class="zjtitle">${article.title}</div>
   <div class="zjimg">
   		<img src="${APP}uploadfile/images/${original}" width="100%"/>
   		<span>${article.labels}</span>
   </div>
   <div class="article-con">${article.details}</div>
</div>
<div class="xg_wz">
	<div class="m">更多专家</div>	
</div>
<div class="xg_list">
  <ul>
    <c:forEach items="${zjlist}" var="zj">
    <li>
      <div class="xg_list_img"><a href="/mobile/index/article?id=${zj.id}" title="${zj.title}">
      <img src="${APP}uploadfile/images/${zj.original}" width="100%"></a></div>
      <div class="xg_list_con">
        <div class="xg_list_con_L">
        	<a href="/mobile/index/article?id=${zj.id}" title="${zj.title}">${zj.title}</a>
        	<p>${zj.desc}</p>
        </div>
        <div class="xg_list_con_R">
        	<a href="/mobile/index/article?id=${zj.id}"><img width="18px" src="${IMG}i17.png"/></a>
        </div>
      </div>
    </li>
    </c:forEach>
  </ul>
</div>
