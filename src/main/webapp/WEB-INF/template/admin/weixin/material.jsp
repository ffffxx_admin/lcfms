<%@ page language="java" contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>	
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<link rel="stylesheet" href="${CSS}bootstrap.min.css"/>
		<link rel="stylesheet" href="${CSS}font-awesome.css"/>
		<link rel="stylesheet" href="${CSS}widgets.css"/>
		<link rel="stylesheet" href="${CSS}b.page.bootstrap3.css"/>
		<script src="${JS}jquery-3.2.1.min.js"></script>
		<script src="${JS}bootstrap.min.js"></script>
		<script src="${JS}widgets.js"></script>
		<script type="text/javascript" src="${JS}b.page.js"></script>
		<style type="text/css">
		.sc-box{margin:0 auto;border:1px solid #c2c2c2;box-shadow: 0px 0px 10px -2px;max-width:300px;}
		.sc-box-top{height:140px;position: relative;padding:12px 15px;}
		.sc-box-top>.ylwz{display:none;line-height:140px;position: absolute;top:0;left:0;text-align: center;font-weight: bold;color: #fff;cursor:pointer;width:100%;height:100%;background:rgba(130, 130, 130, 0.72);}	
		.sc-box-top>p{position: absolute;bottom: 15px;width: 90%;background: rgba(39, 35, 35, 0.56);margin: 0;text-align: center;line-height: 28px;font-weight: bold;color: #fff;}
		.sc-box-next{position: relative;height:60px;padding:8px 15px;border-top:1px solid rgba(183, 183, 183, 0.35);font-size:12px;}
		.sc-box-next i{display: block;width: 50px;height: 50px;position: absolute; right: 0;top: 0;}
		.sc-box-next>.ylwz{display:none;line-height:60px;position: absolute;top:0;left:0;text-align: center;font-weight: bold;color: #fff;cursor:pointer;width:100%;height:100%;background:rgba(130, 130, 130, 0.72);}	
		.sc-box-bottom{font-size:12px;padding:12px 15px;color:grey;}
		</style>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<div class="widget-box  widget-color-blue" id="3061">
					<div class="widget-header widget-header-small">
						<h6 class="widget-title">
							<i class="fa fa-sort">
							</i>
							素材管理
						</h6>
						<div class="widget-toolbar">
							<a href="javascript:void(0);" data-action="fullscreen">
								<i class="fa fa-expand">
								</i>
							</a>
							<a href="javascript:void(0);" data-action="collapse">
								<i class="fa fa-chevron-up">
								</i>
							</a>
							<a href="javascript:void(0);" data-action="close">
								<i class="fa fa-minus">
								</i>
							</a>
						</div>
					</div>
					<div class="widget-body">
						<div class="widget-body-inner" style="display: block;">
						<div class="widget-main" style="padding: 12px;"> 
						   <div class="container-fluid">
						   <div class="row">
						        <c:forEach items="${list}" var="v">
								<div class="col-sm-4">
								    <div class="sc-box">
								    	<c:set var="im" value="0"/>	
								        <c:forEach items="${v.content}" var="s" >
								        <c:if test="${im==0}">
								        <div class="sc-box-top" style="background:url(${s.thumb_url});background-origin: content-box;background-repeat: no-repeat;background-clip: content-box;background-size: 100%;" url="${s.url}">
											<p>${s.title}</p>
											<div class="ylwz">预览文章</div>
										</div>
								        </c:if>
								        <c:if test="${im!=0}">
								        <div class="sc-box-next" url="${s.url}">
											<div class="ct">${s.title}</div>
											<i style="background:url(${s.thumb_url});background-origin: content-box;background-repeat: no-repeat;background-clip: content-box;background-size: 100%;"></i>
											<div class="ylwz">预览文章</div>
										</div>
								        </c:if>
								        <c:set var="im" value="${im+1}"/>	
								        </c:forEach>	
										<div class="sc-box-bottom">
											<div class="ct">更新于 ${v.update_time}</div>
										</div>
									</div>
								</div>	
								</c:forEach>						
							</div>
						    </div>
						</div>
						</div>
					</div>
					<div id="page" pagenumber="1" pagesize="6" totalrow="${count}"></div>	
				</div>
			</div>
		</div>
		<script>					
		$(function(){
			widget.init();
			//当前页	
			var pageNumber=$('#page').attr("pageNumber");
			//每页多少行	
			var pageSize=$('#page').attr("pageSize");
			//总共多少行
			var totalRow=$('#page').attr("totalRow");
			//总共多少页
			var totalPage=Math.ceil(totalRow/pageSize);
			$('#page').bPage({  
			    url : '',
			    totalPage :totalPage,
			    totalRow :totalRow,
			    pageSize :pageSize,
			    pageNumber:pageNumber
			});	
			
			$(".sc-box-top").click(function(){
				var url=$(this).attr("url");
				window.open(url);
			});
			
			$(".sc-box-next").click(function(){
				var url=$(this).attr("url");
				window.open(url);
			});
			
			$(".sc-box-top,.sc-box-next").mouseover(function(){
				$(this).children(".ylwz").show();
			});
			
			$(".sc-box-top,.sc-box-next").mouseout(function(){
				$(this).children(".ylwz").hide();
			});
		
		});
		
		</script>
	</body>
</html>