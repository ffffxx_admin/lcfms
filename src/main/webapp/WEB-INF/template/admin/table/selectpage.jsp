<%@page import="java.util.HashMap"%>
<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="lc" uri="/WEB-INF/tld/lc.tld" %>	
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="${CSS}bootstrap.min.css"/>
	<link rel="stylesheet" href="${CSS}font-awesome.css"/>	
	<link rel="stylesheet" href="${CSS}widgets.css"/>
	<link rel="stylesheet" href="${CSS}b.page.bootstrap3.css" type="text/css">
	<link rel="stylesheet" href="${CSS}form.css">
	<link rel="stylesheet" href="${CSS}kalendae.css"/>
	<script src="${JS}jquery-3.2.1.min.js" type="text/javascript"></script>
	<script src="${JS}../../layer/layer.js"></script>
	<script src="${JS}bootstrap.min.js"></script>
	<script src="${JS}widgets.js" type="text/javascript"></script>
	<script src="${JS}b.page.js" type="text/javascript"></script>
	<script src="${JS}form.js"></script>
	<script src="${JS}kalendae.js"></script>	
</head>
<body>
<div class="container-fluid">
	<div class="row">
	    <c:if test="${widgetTopJsp ne null}">
		<jsp:include page="../${widgetTopJsp}"/>
		</c:if>
		<div class="widget-box  widget-color-blue" id="3061">
			<div class="widget-header widget-header-small">
				<h6 class="widget-title">
					<i class="fa fa-sort">
					</i>
					${title}
				</h6>	
				<div class="widget-toolbar">					
					<c:forEach items="${toolbar}" var="tool">
					<lc:if test="${tool.type eq 'js'}">
					<a href="javascript:void(0);" data-action="${tool.action}">
						<i class="fa ${tool.icon}">
						</i>					
					</a>	
					</lc:if>					
					</c:forEach>
				</div>
				<div class="widget-toolbar no-border">					
					<c:forEach items="${toolbar}" var="tool">					
					<lc:if test="${tool.type eq 'url'}">
					<button onclick="location.href='${tool.url}';" class="btn btn-xs btn-default smaller">
						<i class="fa ${tool.icon}"></i> ${tool.title}
					</button>
					</lc:if>
					<lc:elseif test="${tool.type eq 'tab'}">
					<button class="btn btn-xs btn-default smaller" onclick="openNewTab(${tool.itemId},'${tool.url}','${tool.itemName}');">
						<i class="fa ${tool.icon}"></i> ${tool.title}
					</button>
					</lc:elseif>
					<lc:elseif test="${tool.type eq 'layer'}">
					<button class="btn btn-xs btn-default smaller" onclick="openlayer('${tool.title}','${tool.icon}','${tool.url}',${tool.width},${tool.height});">
						<i class="fa ${tool.icon}"></i> ${tool.title}
					</button>
					</lc:elseif>
					</c:forEach>
				</div>							
			</div>
			<div class="widget-body">
				<div class="widget-body-inner" style="display: block;">
					<div class="widget-main"  style="padding: 12px;">
					    <c:if test="${tableTopJsp ne null}">
						<jsp:include page="../${tableTopJsp}"/>
						</c:if>
					    <c:if test="${filter.size()>0}">
						<form class="form-horizontal" name="myform" method="get" action="${APP}${pageurlpath}">
						<div class="form-body">
							<c:forEach var="i" begin="0" end="${filter.size()}">
							<c:forEach var="valm" items="${filter}">
							<c:if test="${valm.sort==i}">
						    <div class="col-sm-4">
							<div class="form-group">
								<label class="col-sm-4 control-label">
									${valm.title}：
								</label>
								<div class="col-sm-8">
									<c:if test="${valm.form eq 'input'}">
										<input class="form-control input-sm" name="${valm.name}" type="text" value="${valm.value}">
									</c:if>
									<c:if test="${valm.form eq 'select'}">
									<select class="form-control" name="${valm.name}">
									    <c:set var="i" value="0"/>	
										<c:forEach items="${valm.values}" var="formitem">																		
										<option value="${formitem}" <c:if test="${valm.value eq formitem}">selected</c:if>>
										<c:out value="${valm.text[i]}"/>								
										</option>
										<c:set var="i" value="${i+1}"/>	
										</c:forEach>											
									</select>					
									</c:if>		
									<c:if test="${valm.form eq 'time'}">
										<input readonly="readonly" class="form-control calendar" value="${valm.value}" name="${valm.name}" type="text" months="1" mode="single" direction="">      					
									</c:if>			
								</div>
							</div>
							</div>
							</c:if>
							</c:forEach>
							</c:forEach>
						    <div class="col-sm-4">
							    <div class="form-group">
							    <label class="col-sm-4 control-label"></label>
							    <div class="col-sm-8"><button type="button" id="save" class="btn btn-sm btn-grey"><i class="fa fa-check"></i> 搜索</button></div>								
							    </div>
							</div>	
						</div>												
						</form>	
						<script>
						var myform=form.init("myform");	
						$("#save").click(function(){
							myform.submit();
						});
						</script>
						</c:if>
						<lc:if test="${datalist.size()==0}">
							<div style="text-align:center;font-size:22px;clear:both;">无数据</div>
						</lc:if>
						<lc:else>
						<table id="tableBox" class="bTable table table-striped table-bordered table-hover table-condensed">
							<thead>
								<tr>
									<c:forEach items="${column}" var="col">		
									    <c:if test="${columnWidth.get(col)!=0}">
										<lc:if test="${sortColumn.contains(col)}">
										    <c:if test="${col.concat('_asc') eq sort}">
											<th width="${columnWidth.get(col)}px"><label class="sortColumn" column="${col}" sort="asc"><i class="fa fa-sort-asc"></i>&nbsp;${col}</label></th>
										    </c:if>
										    <c:if test="${col.concat('_desc') eq sort}">
											<th width="${columnWidth.get(col)}px"><label class="sortColumn" column="${col}" sort="desc"><i class="fa fa-sort-desc"></i>&nbsp;${col}</label></th>
										    </c:if>
										    <c:if test="${col.concat('_asc') ne sort && col.concat('_desc') ne sort}">
											<th width="${columnWidth.get(col)}px"><label class="sortColumn" column="${col}" sort=""><i class="fa fa-sort"></i>&nbsp;${col}</label></th>
										    </c:if>										
										</lc:if>
										<lc:else>
										<th width="${columnWidth.get(col)}px">${col}</th>
										</lc:else>	
										</c:if>														
									</c:forEach>
									<c:if test="${actionButton}"><th style="min-width:120px;">操作</th></c:if>
								</tr>
							</thead>
							<tbody>
							<c:forEach items="${datalist}" var="val">
								<tr>							    
								    <c:forEach items="${column}" var="col1">
								    <c:if test="${columnWidth.get(col1)!=0}">
									<td>${val.get(col1)}</td>
									</c:if>
									</c:forEach>
									<c:if test="${actionButton}">					
									<td>
									<c:forEach items="${edit}" var="col2">
									    <lc:if test="${col2.type eq 'url'}">
									    <a href="${col2.action}${val.get(col2.primaryKey)}"><i class="fa ${col2.icon} bigger-120"></i> ${col2.name}</a>&nbsp;&nbsp;	
									    </lc:if>
										<lc:elseif test="${col2.type eq 'tab'}">
										<a href="javascript:openNewTab(${col2.itemId},'${col2.action}${val.get(col2.primaryKey)}','${col2.itemName}');"><i class="fa ${col2.icon} bigger-120"></i> ${col2.name}</a>&nbsp;&nbsp;
										</lc:elseif> 
										<lc:elseif test="${col2.type eq 'layer'}">
										<a href="javascript:openlayer('${col2.name}','${col2.icon}','${col2.action}${val.get(col2.primaryKey)}',${col2.width},${col2.height});"><i class="fa ${col2.icon} bigger-120"></i> ${col2.name}</a>&nbsp;&nbsp;
										</lc:elseif>   			
									</c:forEach>
									</td>
									</c:if>
								</tr>
							 </c:forEach>							
							</tbody>
						</table>
						<c:if test="${tableBottomJsp ne null}">
						<jsp:include page="../${tableBottomJsp}"/>
						</c:if>
						</lc:else>					
				</div>
				</div>
			</div>
			<div id="page" pagenumber="${pagenumber}" pagesize="${pagesize}" totalrow="${totalrow}"></div>	
		</div>
		<c:if test="${widgetBottomJsp ne null}">
		<jsp:include page="../${widgetBottomJsp}"/>
		</c:if>
	</div>
</div>
<script>
widget.init();
$(function(){
	//设置分页
	var pageNumber=$('#page').attr("pageNumber");
	var pageSize=$('#page').attr("pageSize");
	var totalRow=$('#page').attr("totalRow");
	var totalPage=Math.ceil(totalRow/pageSize);
	$('#page').bPage({  
	    url : '${APP}${pageurlpath}${pageurlquery}',
	    totalPage :totalPage,
	    totalRow :totalRow,
	    pageSize :pageSize,
	    pageNumber:pageNumber,
	    pageSizeMenu:[pageSize,pageSize*2,pageSize*4,pageSize*8]
	});	
	//设置排序
	$(".sortColumn").click(function(){
		var column=$(this).attr("column");
		var sort=$(this).attr("sort");
		if(sort==""){
			location.href="${APP}${pageurlpath}?sort_asc="+column;
		}
		if(sort=="asc"){
			location.href="${APP}${pageurlpath}?sort_desc="+column;
		}
		if(sort=="desc"){
			location.href="${APP}${pageurlpath}?sort_asc="+column;
		}
	});
	//图片变大
	$("#tableBox img").click(function(){
		var json={pid:$(this).attr("pid"),alt:$(this).attr("alt"),src:$(this).attr("original"),thumb:$(this).attr("src")};		
		layer.photos({
		    photos: {
	    	  "title": "查看图片", 
	    	  "id": 1,
	    	  "start": 0, 
	    	  "data":[json]
		   	}
		});
	});	
});

function openNewTab(itemId,url,itemName){
	if(url.indexOf("http")==-1){
		var c="${USERINFO.mca[1]}".toLowerCase();
		url="${APP}${USERINFO.mca[0]}/"+c+"/"+url;
	}
	parent.openNewTab(itemId,url,itemName)	
}

function openlayer(title,icon,url,width,height){
	if(title=='删除' && icon=='fa-trash'){
		layer.confirm('您确定要删除吗？',{btn: ['是的','取消']}, function(){
			 location.href=url;
		});
	}else{
		layer.open({
		      type: 2,
		      title: '<i class="fa '+icon+'"></i> '+title,
		      maxmin: true,
		      shadeClose: true, //点击遮罩关闭层
		      area : [width+'px' , height+'px'],
		      content: url
		});
	}	
}
</script>
</body>
</html>
 