<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script>
function checkname(table,id){
	$.post("checkFileType",{table:table,id:id},function(res){
		if(res==0){
			myform.error("file_type_id","表"+table+"不存在该主题！");
		}else{
			myform.success("file_type_id",res);
		}			
	});
}
$("input[name='file_type_id']").blur(function(){
	var t=Number($(this).val());
	var table=$("select[name='file_type']").val();
	if(!isNaN(t)){
		checkname(table,t);
	}
});

$("select[name='file_type']").change(function(){
	var t=Number($("input[name='file_type_id']").val());
	var table=$(this).val();
	if(!isNaN(t)){
		checkname(table,t);
	}
});
</script>