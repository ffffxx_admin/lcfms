<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="${CSS}bootstrap.min.css"/>
	<link rel="stylesheet" href="${CSS}font-awesome.css"/>	
	<link rel="stylesheet" href="${CSS}jquery-ui.min.css"/>	
	<link rel="stylesheet" href="${CSS}slider.css"/>	
	<script src="${JS}jquery-3.2.1.min.js"></script>
	<script src="${JS}bootstrap.min.js"></script>
	<script src="${JS}jquery-ui.min.js"></script>
</head>
<body style="padding:50px 15px;">
<div class="container-fluid">
<div class="row">
    <div class="col-xs-12">
		<div class="slider" id="slider-range"></div> 
		<script type="text/javascript">
		function slider(){
			$("#slider-range").addClass("ui-slider-green");	
			$("#slider-range").empty().css('width','400px').slider({
				orientation: 'horizontal',
				range: 'min',
				min: 0,
				max: 100,
				step: 5,
				value:50,
				slide: function( event, ui ) {
					var val = parseInt(ui.value);	
					if(val<0){
						return false;
					}
					if(val>90){
						return false;
					}			
					if(!ui.handle.firstChild) {											
						$(ui.handle).append("<div class='tooltip top in' role='tooltip' style='display:none;left:-21px;top:-35px;'><div class='tooltip-arrow'></div><div class='tooltip-inner' style='width:60px'></div></div>");							
					}								
					$(ui.handle.firstChild).show().children().eq(1).text("第"+val+"个");						
				}
			});
			$("#slider-range").children(".ui-slider-handle").blur(function(){
				$(this).children(".tooltip").hide();
			});
		}		
		</script>
	</div>
</div>
</div>
<script type="text/javascript">slider();</script>
</body>
</html>
 