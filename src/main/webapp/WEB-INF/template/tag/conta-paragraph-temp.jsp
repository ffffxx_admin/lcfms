<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="${CSS}bootstrap.min.css"/>
	<link rel="stylesheet" href="${CSS}font-awesome.css"/>	
	<link rel="stylesheet" href="${CSS}paragraph.css"/>
	<script src="${JS}jquery-3.2.1.min.js"></script>
</head>
<body>
<div class="container-fluid" id="appendHtml">
<div class="row">         
	 <div class="paragraph paragraph-success">
		 <div class="paragraph-top">
		 	<button type="button" class="close">
				<i class="fa fa-remove"></i>
			</button>
		 </div>	
		 <div class="paragraph-body index">
		 	<p>内容</p>
		 </div>	
		 <script>
		 	$(".paragraph-top>button[class='close']").click(function(){
		 		$(this).parent().parent().remove();
		 	});
		 </script>
	</div>	
</div>  
</div>
</body>
</html>    
