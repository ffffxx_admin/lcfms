<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="${CSS}bootstrap.min.css" />
	<link rel="stylesheet" href="${CSS}font-awesome.min.css"/>	
	<link rel="stylesheet" href="${CSS}ace.min.css" />
	<link rel="stylesheet" href="${CSS}edittag.css" />
	<!--[if !IE]> -->
	<script src="${JS}jquery-2.1.4.min.js"></script>
	<!-- <![endif]-->	
	<!--[if IE]>
	<script src="${JS}jquery-1.11.3.min.js"></script>
	<![endif]-->	
	<script src="${JS}edittag.js"></script>
	<script src="${JS}bootstrap.min.js"></script>
	<script src="${JS}ace-elements.min.js"></script>
	<script src="${JS}ace.min.js"></script>
	<script src="${JS}showtag.js"></script>
</head>
<body style="background:#fff;">
<form name="Editform">
<div class="form-group">
	<label class="control-label" type="text">标题：</label>
	<div class="controls">
		<input placeholder="标题" type="text" name="title" value="多项选择"/>
	</div>
</div>
<div id="tablelinegroup">
	<div class="form-group tableline">
		<label class="control-label" type="text">第1项：</label>
		<div class="controls">
			名称-<input type="text" class="linename" style="width:30%" value="选项 1"/> value-<input type="text" class="linevalue" style="width:30%"  value="1"/>			
		</div>
	</div>
	<div class="form-group tableline">
		<label class="control-label" type="text">第2项：</label>
		<div class="controls">
			名称-<input type="text" class="linename" style="width:30%" value="选项 2"/> value-<input type="text" class="linevalue" style="width:30%"  value="2"/>		
		</div>
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text">name：</label>
	<div class="controls">
		<div class="clearfix">
			<input type="text" name="formname" placeholder="name" value="laocheng"/>
		</div>		
	</div>
</div>
</form>
<div class="form-group">
	<label class="control-label" type="text"></label>
	<div class="controls">
		<button class="btn btn-xs btn-success addline"><i class="icon-plus"></i>增加选项</button>
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text"></label>
	<div class="controls">
		 <button class="btn btn-sm btn-primary btn-block" onclick="saveEdit();">保存效果</button>
	</div>
</div> 
<div id="hidetableline" style="display:none;">
	<div class="form-group tableline">
		<label class="control-label" type="text"></label>
		<div class="controls">
			名称-<input type="text" class="linename" style="width:30%" value="选项 3"/> value-<input type="text" class="linevalue" style="width:30%" value="3"/>
			<a class="red" href="#" onclick="deleteline(this);">
				<i class="icon-trash bigger-130"></i>
			</a>
		</div>
	</div>
</div>
<script type="text/javascript">     
	function deleteline(Element){
		$(Element).parent().parent().remove();
	    countline();	
	}
	function countline(){
		var len=$("#tablelinegroup").find(".tableline");
		for(var i=0;i<len.length;i++){
			$(len[i]).find("label").html("第"+(i+1)+"列：");
			if((i+1)==len.length){
			var name=$(len[i]).find(".linename").val("选项 "+(i+1));
			var val=$(len[i]).find(".linevalue").val(i+1);
			}	
		}
	}
	$(".addline").click(function(){
	   var hidetableline=$("#hidetableline").html();
	   $("#tablelinegroup").append(hidetableline);
	   countline();	   
	});
	
	function saveEdit(){
		  var modalElement=getEditHtml();
		  var title=document.Editform.title.value;
		  modalElement.children().find(".control-label:last").html(title+"：");	
		  var len=$("#tablelinegroup").find(".tableline");
		  var str="";
		  var d="";
		  var formname=document.Editform.formname.value;
		  for(var i=0;i<len.length;i++){
			var name=$(len[i]).find(".linename").val();
			var val=$(len[i]).find(".linevalue").val();
			str+="<label class=\"checkbox-inline\"><input name=\""+formname+"\" value=\""+val+"\" type=\"checkbox\"/><span class=\"lbl\"> "+name+"</span></label> \n";	
		  }  	  
		  modalElement.children().find(".checkbox").html(str);	
    }

</script>
</body>
</html>

