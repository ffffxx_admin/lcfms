<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="${CSS}bootstrap.min.css" />
	<link rel="stylesheet" href="${CSS}font-awesome.min.css"/>	
	<link rel="stylesheet" href="${CSS}ace.min.css" />
	<link rel="stylesheet" href="${CSS}edittag.css" />
	<link rel="stylesheet" href="${CSS}bootstrap-switch.css" />
	<!--[if !IE]> -->
	<script src="${JS}jquery-2.1.4.min.js"></script>
	<!-- <![endif]-->	
	<!--[if IE]>
	<script src="${JS}jquery-1.11.3.min.js"></script>
	<![endif]-->	
	<script src="${JS}edittag.js"></script>
	<script src="${JS}bootstrap.min.js"></script>
	<script src="${JS}ace-elements.min.js"></script>
	<script src="${JS}ace.min.js"></script>
	<script src="${JS}showtag.js"></script>	
	<script src="${JS}bootstrap-switch.js"></script>
</head>
<body style="background:#fff;">
<form name="Editform">
<div class="form-group">
	<label class="control-label" type="text">文字：</label>
	<div class="controls">
		<input type="text" value="按钮" name="text"/>	
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text">图标：</label>
	<div class="controls">
		<div class="switch" id="mySwitch" data-on-label="是" data-off-label="否" on="true">
		    <input type="checkbox" name="iconcheck" checked />
		</div>
	</div>
</div>
<div class="form-group" id="iconform">
	<label class="control-label" type="text">icon图标：</label>
	<div class="controls">
		<input type="text" value="fa fa-check" name="icon"/>	
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text">按钮大小：</label>
	<div class="controls">
		<select class="form-control input-medium" name="big">
			<option value="btn-block">块按钮</option>
			<option value="btn-lg">大按钮</option>
			<option value="" selected>普通按钮</option>	
			<option value="btn-sm">小按钮</option>
			<option value="btn-xs">超小按钮</option>																											
		</select>
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text">按钮样式：</label>
	<div class="controls">
		<select class="form-control input-medium" name="style">
		    <option value="btn-grey">灰底grey</option>
			<option value="btn-default">白底default</option>
			<option value="btn-primary" selected>蓝底primary</option>	
			<option value="btn-success">绿底success</option>
			<option value="btn-info">淡蓝info</option>
			<option value="btn-warning">黄底warning</option>
			<option value="btn-danger">红底danger</option>														
		</select>
	</div>
</div>
</form>
<div class="form-group">
	<label class="control-label" type="text"></label>
	<div class="controls">
		 <button class="btn btn-sm btn-primary btn-block" onclick="saveEdit();">保存效果</button>
	</div>
</div>
<script type="text/javascript">  
	$('#mySwitch').on('switch-change', function (e, data) {
	    var value = data.value;
	    if(value){
	    	$("#iconform").removeClass("hide");
	    }else{
	    	$("#iconform").addClass("hide");
	    }
	});
	function saveEdit(){
	   var modalElement=getEditHtml();
	   var bagae=$("input[name=iconcheck]:checked").val();
	   var icon="";
	   if(bagae=="on"){
		   icon=document.Editform.icon.value;
		   icon="<i class=\""+icon+" align-top bigger-125\"></i>";
	   }
	   var style=document.Editform.style.value;
	   var big=document.Editform.big.value;
	   modalElement.children().find("button").attr("class","btn "+big+" "+style);
	   var text=document.Editform.text.value;
	   modalElement.children().find("button").html(icon+" "+text);
	  
	}
</script>
</body>
</html>
