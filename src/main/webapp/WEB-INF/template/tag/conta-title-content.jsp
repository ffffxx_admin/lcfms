<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="${CSS}bootstrap.min.css" />
	<link rel="stylesheet" href="${CSS}font-awesome.min.css"/>	
	<link rel="stylesheet" href="${CSS}ace.min.css" />
	<link rel="stylesheet" href="${CSS}edittag.css" />
	<!--[if !IE]> -->
	<script src="${JS}jquery-2.1.4.min.js"></script>
	<!-- <![endif]-->	
	<!--[if IE]>
	<script src="${JS}jquery-1.11.3.min.js"></script>
	<![endif]-->	
	<script src="${JS}edittag.js"></script>
	<script src="${JS}bootstrap.min.js"></script>
	<script src="${JS}ace.min.js"></script>
	<script src="${JS}ace-elements.min.js"></script>
	<script src="${JS}showtag.js"></script>
</head>
<body style="background:#fff;">
<form name="Editform">
<div class="form-group">
	<label class="control-label" type="text">头部ICON：</label>
	<div class="controls">
		<input type="text" name="iconsort" value="fa fa-sort"/>
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text">背景色：</label>
	<div class="controls">
	<select id="simple-colorpicker-1" class="hide">
		<option data-class="blue" value="#307ECC">#307ECC</option>
		<option data-class="blue2" value="#5090C1">#5090C1</option>
		<option data-class="blue3" value="#6379AA">#6379AA</option>
		<option data-class="green" value="#82AF6F">#82AF6F</option>
		<option data-class="green2" value="#2E8965">#2E8965</option>
		<option data-class="green3" value="#5FBC47">#5FBC47</option>
		<option data-class="red" value="#E2755F">#E2755F</option>
		<option data-class="red2" value="#E04141">#E04141</option>
		<option data-class="red3" value="#D15B47">#D15B47</option>
		<option data-class="orange" value="#FFC657">#FFC657</option>
		<option data-class="purple" value="#7E6EB0">#7E6EB0</option>
		<option data-class="pink" value="#CE6F9E">#CE6F9E</option>
		<option data-class="dark" value="#404040">#404040</option>
		<option data-class="grey" value="#848484">#848484</option>
		<option data-class="default" selected value="#EEE">#EEE</option>
	</select>
	</div>	
</div>
<div class="form-group">
	<label class="control-label" type="text">内边距：</label>
	<div class="controls">
		<input type="text" name="padding" value="12"/>
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text">头部框：</label>
	<div class="controls">
		<div class="radio">
		<label>
			<input name="useborder" type="radio"  class="ace" value="1" checked/>
			<span class="lbl"> 小框</span>
		</label>
		<label>
			<input name="useborder" type="radio"  class="ace" value="0"/>
			<span class="lbl"> 大框</span>
		</label>
		</div>
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text">扩展选项：</label>
	<div class="controls">
		<div class="checkbox">
			<label>
				<input name="topelse" type="checkbox" class="ace" value="fa fa-bars" dataaction="menu" datafont="菜单"/>
				<span class="lbl"> 菜单</span>
			</label>	
			<label>
				<input name="topelse" type="checkbox" class="ace" value="fa fa-cog" dataaction="settings" datafont="设置"/>
				<span class="lbl"> 设置</span>
			</label>			
			<label>
				<input name="topelse" type="checkbox" class="ace" value="fa fa-save" dataaction="save" datafont="保存"/>
				<span class="lbl"> 保存</span>
			</label>	
			<label>
				<input name="topelse" type="checkbox" class="ace" value="fa fa-trash" dataaction="delete" datafont="删除"/>
				<span class="lbl"> 删除</span>
			</label>
		    <label>
				<input name="topelse" type="checkbox" class="ace" value="fa fa-edit" dataaction="edit" datafont="编辑"/>
				<span class="lbl"> 编辑</span>
			</label>	
			<label>
				<input name="topelse" type="checkbox" class="ace" value="fa fa-reply" dataaction="reply" datafont="返回"/>
				<span class="lbl"> 返回</span>
			</label>
			<label>
				<input name="topelse" type="checkbox" checked class="ace" value="fa fa-expand" dataaction="fullscreen" datafont="缩放"/>
				<span class="lbl"> 缩放</span>
			</label>
			<label>
				<input name="topelse" type="checkbox" checked class="ace" value="fa fa-refresh" dataaction="reload" datafont="刷新"/>
				<span class="lbl"> 刷新</span>
			</label>
			<label>
				<input name="topelse" type="checkbox" checked class="ace" value="fa fa-chevron-up" dataaction="collapse" datafont="缩小"/>
				<span class="lbl"> 关闭</span>
			</label>	
			<label>
				<input name="topelse" type="checkbox" checked class="ace" value="fa fa-minus" dataaction="close" datafont="关闭"/>
				<span class="lbl"> 缩小</span>
			</label>					
		</div>	
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text">扩展提示：</label>
	<div class="controls">
		<div class="radio">
		<label>
			<input name="tooltip" type="radio"  class="ace" value="1"/>
			<span class="lbl"> 是</span>
		</label>
		<label>
			<input name="tooltip" type="radio"  class="ace" value="0" checked/>
			<span class="lbl"> 否</span>
		</label>
		</div>
	</div>
</div>
</form>
<div class="form-group">
	<label class="control-label" type="text"></label>
	<div class="controls">
		 <button class="btn btn-sm btn-primary btn-block" onclick="saveEdit();">保存效果</button>
	</div>
</div>
<script type="text/javascript">												
    $(document).ready(function(){
		$('#simple-colorpicker-1').ace_colorpicker().on('change', function(){
			var modalElement=getEditHtml();
			var color_class = $(this).find('option:selected').data('class');
			if(color_class != 'default'){
			var new_class = 'widget-box  widget-color-'+color_class;
			modalElement.children().find(".widget-box").attr("class",new_class);
			}	
		});
    });
	
	function saveEdit(){
		  var modalElement=getEditHtml();
		  var iconsort=document.Editform.iconsort.value;	
		  modalElement.children().find(".widget-header>h6").html("<i class=\""+iconsort+"\"></i> 标题内容");
		  var padding=document.Editform.padding.value;	     
		  modalElement.children().find(".widget-main").css("padding",padding);
		  var indata="";
		  var l=$("input[name=topelse]:checked").length;
		  $("input[name=topelse]:checked").each(function(i){
		  	 var javascript="";		
			 var icon=$(this).val();
			 var action=$(this).attr("dataaction");
			 var datafont=$(this).attr("datafont");
			 var tooltip=document.Editform.tooltip.value;
			 var placement="top";
			 if(i==(l-1)){
				 placement="left"; 
			 }
			 if(action=='menu'){
			 	if(tooltip==1){
				 	var instr="<a href=\"javascript:void(0);\" data-toggle=\"dropdown\" aria-expanded=\"false\" data-rel=\"tooltip\" data-placement=\""+placement+"\" data-original-title=\""+datafont+"\" data-action=\""+action+"\"><i class=\""+icon+"\"></i></a>\n"; 
				}else{
					var instr="<a href=\"javascript:void(0);\" data-toggle=\"dropdown\" aria-expanded=\"false\" data-action=\""+action+"\"><i class=\""+icon+"\"></i></a>\n";
				}	
				instr+="<ul class=\"dropdown-menu dropdown-light-blue dropdown-caret dropdown-closer\">";
				instr+="<li><a href=\"javascript:void(0);\"><i class=\"fa fa-plus\"></i>&nbsp;&nbsp;测试列表一</a></li>";
				instr+="<li><a href=\"javascript:void(0);\"><i class=\"fa fa-plus\"></i>&nbsp;&nbsp;测试列表二</a></li></ul>";
			 }else{
			 	if(tooltip==1){
				 	var instr="<a href=\"javascript:void(0);\" data-rel=\"tooltip\" data-placement=\""+placement+"\" data-original-title=\""+datafont+"\" data-action=\""+action+"\"><i class=\""+icon+"\"></i></a>\n"; 
				}else{
					var instr="<a href=\"javascript:void(0);\" data-action=\""+action+"\"><i class=\""+icon+"\"></i></a>\n";
				}	
			 }			
			 indata+=instr;
		  });
		  modalElement.children().find(".widget-toolbar").html(indata);
		  var useborder=document.Editform.useborder.value;	
		  if(useborder==1){
			  modalElement.children().find(".widget-header").addClass("widget-header-small");
		  }else{
			  modalElement.children().find(".widget-header").removeClass("widget-header-small");
		  }
		  getEditWindow().widget.init();
	}
</script>
</body>
</html>
