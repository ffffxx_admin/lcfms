<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="${CSS}bootstrap.min.css" />
	<link rel="stylesheet" href="${CSS}font-awesome.min.css"/>	
	<link rel="stylesheet" href="${CSS}ace.min.css" />
	<link rel="stylesheet" href="${CSS}edittag.css" />
	<!--[if !IE]> -->
	<script src="${JS}jquery-2.1.4.min.js"></script>
	<!-- <![endif]-->	
	<!--[if IE]>
	<script src="${JS}jquery-1.11.3.min.js"></script>
	<![endif]-->	
	<script src="${JS}edittag.js"></script>
	<script src="${JS}bootstrap.min.js"></script>
	<script src="${JS}ace-elements.min.js"></script>
	<script src="${JS}ace.min.js"></script>
	<script src="${JS}showtag.js"></script>
</head>
<body style="background:#fff;">
<form name="Editform">
<div class="form-group">
	<label class="control-label" type="text">文字：</label>
	<div class="controls">
		<input type="text" value="徽标" name="text"/>	
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text">类型：</label>
	<div class="controls">
		<div class="radio">
		<label>
			<input name="bagae" type="radio"  class="ace" value="label" checked/>
			<span class="lbl"> 角标</span>
		</label>
		<label>
			<input name="bagae" type="radio"  class="ace" value="badge"/>
			<span class="lbl"> 徽标</span>
		</label>
		</div>
	</div>
</div>
<div class="form-group">
	<label class="control-label" type="text">段落样式：</label>
	<div class="controls">
		<select class="form-control input-medium" name="style">
			<option value="label-default">灰底default</option>
			<option value="label-primary">蓝底primary</option>	
			<option value="label-success" selected>绿底success</option>
			<option value="label-info">淡蓝info</option>
			<option value="label-warning">黄底warning</option>
			<option value="label-danger">红底danger</option>														
		</select>
	</div>
</div>
</form>
<div class="form-group">
	<label class="control-label" type="text"></label>
	<div class="controls">
		 <button class="btn btn-sm btn-primary btn-block" onclick="saveEdit();">保存效果</button>
	</div>
</div>
<script type="text/javascript">  
	function saveEdit(){
	   var modalElement=getEditHtml();
	   var bagae=$("input[name=bagae]:checked").val();	
	   var style=document.Editform.style.value;
	   modalElement.children().find("span").attr("class",style+" "+bagae);
	   var text=document.Editform.text.value;
	   modalElement.children().find("span").html(text);
	}
</script>
</body>
</html>
