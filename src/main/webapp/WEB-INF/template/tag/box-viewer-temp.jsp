<%@ page language="java" contentType="text/html"  pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" href="${CSS}bootstrap.min.css"/>
	<script src="${JS}jquery-3.2.1.min.js"></script>
	<script src="${JS}../../layer/layer.js"></script>
	<style>
	   img{cursor:pointer;float:left;margin:5px;}
	</style>
</head>
<body style="text-align:center;padding-top:30px;">
<div class="container-fluid">
<div class="row">
	<img pid="1" src="${APP}statics/ace/images/gallery/thumb-1.jpg" alt="图片1">
	<img pid="2" src="${APP}statics/ace/images/gallery/thumb-2.jpg" alt="图片2">
	<img pid="3" src="${APP}statics/ace/images/gallery/thumb-3.jpg" alt="图片3">
	<img pid="4" src="${APP}statics/ace/images/gallery/thumb-4.jpg" alt="图片4">
	<img pid="5" src="${APP}statics/ace/images/gallery/thumb-5.jpg" alt="图片5">
</div>
</div>
<script>
$("img").click(function(){
	var img1={pid:0,alt:'图片1',src:'${APP}statics/ace/images/gallery/image-1.jpg',thumb:'${APP}statics/ace/images/gallery/thumb-1.jpg'};		
	var img2={pid:1,alt:'图片2',src:'${APP}statics/ace/images/gallery/image-2.jpg',thumb:'${APP}statics/ace/images/gallery/thumb-2.jpg'};		
	var img3={pid:2,alt:'图片3',src:'${APP}statics/ace/images/gallery/image-3.jpg',thumb:'${APP}statics/ace/images/gallery/thumb-3.jpg'};		
	var img4={pid:3,alt:'图片4',src:'${APP}statics/ace/images/gallery/image-4.jpg',thumb:'${APP}statics/ace/images/gallery/thumb-4.jpg'};		
	var img5={pid:4,alt:'图片5',src:'${APP}statics/ace/images/gallery/image-5.jpg',thumb:'${APP}statics/ace/images/gallery/thumb-5.jpg'};		
	layer.photos({
	    photos: {
    	  "title": "查看图片", 
    	  "start": $(this).attr("pid"), 
    	  "data":[img1,img2,img3,img4,img5]
	   	}
	});
});	
</script>
</body>
</html>
 