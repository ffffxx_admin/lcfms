/**
 * Created by user on 2017/4/3.
 */
$(function(){
	
    /* 专家切换 */
    var oTit = document.getElementById('main3_tit');
    var aDiv = oTit.getElementsByTagName('div');
    var oUl = document.getElementById('main3_ul');
    var aLi = oUl.getElementsByTagName('li');
    var expertOn=0;
    for( var i = 0; i < aDiv.length; i ++ ){
        aDiv[i].index = i;
        aDiv[i].addEventListener('touchstart',function(){
            for (var i = 0; i < aDiv.length; i++) {
                aDiv[i].className = 'swiper-slide';
                aLi[i].className = "active";
            }
            expertOn=this.index;
            this.className = 'swiper-slide active';
            aLi[this.index].className = '';
        });
    }
    function test(){
        expertOn++;
        if(expertOn==aDiv.length)expertOn=0;
        for (var i = 0; i < aDiv.length; i++) {
            aDiv[i].className = 'swiper-slide';
            aLi[i].className = "active";
        }
        aDiv[expertOn].className = 'swiper-slide active';
        aLi[expertOn].className = '';
    }
    setInterval(function(){test()},9000)
})
