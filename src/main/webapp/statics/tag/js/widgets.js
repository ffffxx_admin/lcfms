(function($){
	function getwidth(r){
		var w=widget.widgetjson;
		var id=widget.widgetjson.id;
		var width=widget.widgetjson.width;		
		for(var i=0;i<id.length;i++){
			if(id[i]==r){
				return w.width[i];
			}
		}
	}
	function getheight(r){
		var w=widget.widgetjson;
		var id=widget.widgetjson.id;
		var height=widget.widgetjson.height;
		for(var i=0;i<id.length;i++){
			if(id[i]==r){
				return w.height[i];
			}
		}
	}
	function resetwz(){
		var s=0;
		$('.widget-box').each(function(){
			if($(this).hasClass("smallwidget")){
				$(this).css("right",s*150+s*10);
				s++;
			}
		});
	}
	window.widget={
		widgetjson:{
			minus:0,
			width:[],
			height:[],
			id:[]
		},
		show:function(r){
			var minus=widget.widgetjson.minus;
            var w=$("#"+r);	
			w.width("");
			w.height("");
			w.css("right","initial");
			w.css("bottom","initial");
			w.removeClass("smallwidget");
			w.children().find(".smallwidget").remove();	
			widget.widgetjson.minus--;	
            resetwz();			
		},
		
		random:function (min,max){
			return Math.floor(min+Math.random()*(max-min));
		},		
        init:function(){
			$('[data-rel=tooltip]').tooltip();
			$('.widget-box').each(function(i){
				var r=widget.random(1000,9999)	
				widget.widgetjson.id[i]=r;
				widget.widgetjson.width[i]=$(this).children(".widget-body").width();
				widget.widgetjson.height[i]=$(this).children(".widget-body").height();
                $(this).attr("id",r);		
			});
			
			$('.widget-toolbar>a[data-action=close]').click(function(){
				resetwz();			
				var minus=widget.widgetjson.minus;				
				var w=$(this).parent().parent().parent();
				if(w.hasClass("fullscreen")){
					w.removeClass("fullscreen");
					w.css("right","initial");
					w.css("bottom","initial");
					w.children().find(".widget-toolbar>a[data-action=fullscreen]").html("<i class=\"fa fa-resize-full\"></i>");
				}				
				w.animate({
				   width: 150, height: 30
				}, 500);
				var l=w[0].getBoundingClientRect().left;
				var t=w[0].getBoundingClientRect().top;
				w.addClass("smallwidget");
				w.css("right",l);
				w.css("bottom",t);			
				w.animate({
				   bottom: 0,right:minus*150+minus*10
				}, 500);   				
				widget.widgetjson.minus++;
                var h=$(this).parent().parent();
				var id=w.attr("id");
                h.children("h6").after("<i class=\"fa fa-clone smallwidget\" style=\"margin-left:50px;\" onclick=\"widget.show("+id+");\"></i>");								
			});
				
			$('.widget-toolbar>a[data-action=collapse]').click(function(){
				var r=$(this).parent().parent().parent().attr("id");
				var w=$(this).parent().parent().parent().children(".widget-body");	
				if($(this).html()=='<i class=\"fa fa-chevron-down\"></i>'){
					w.css("overflow","initial");
					w.animate({
					   height:getheight(r)
					}, 500);  
					$(this).html("<i class=\"fa fa-chevron-up\"></i>");
				}else{
					w.css("overflow","hidden");
					w.animate({
					   height: 0
					}, 500);  
					$(this).html("<i class=\"fa fa-chevron-down\"></i>");
				}						
			});	
			
			$('.widget-toolbar>a[data-action=reload]').click(function(){
				var w=$(this).parent().parent().parent();								
				w.addClass("position-relative");
				w.append("<div class=\"widget-box-overlay\"><i class=\"loading-icon fa fa-spinner fa-spin fa-2x white\"></i></div>");
				alert('在这里写入刷新时需要执行的更多操作');
				setTimeout(function(){
					w.removeClass("position-relative");
					w.children(".widget-box-overlay").remove();	
				},2000)					
			});
			
			$('.widget-toolbar>a[data-action=fullscreen]').click(function(){
				var w=$(this).parent().parent().parent();
				if(w.hasClass("fullscreen")){
					w.removeClass("fullscreen");
					w.css("right","initial");
					w.css("bottom","initial");
					$(this).html("<i class=\"fa fa-expand\"></i>");
				}else{
					w.addClass("fullscreen");
					w.css("right",0);
					w.css("bottom",0);
					$(this).html("<i class=\"fa fa-compress\"></i>");
				}				
			});	
				
			$('.widget-toolbar>a[data-action=settings]').click(function(){
				alert("点击设置后更多操作,可以到widgets.js内添加");
			});
			
			$('.widget-toolbar>a[data-action=reply]').click(function(){
				history.go(-1);
			});
			
			$('.widget-toolbar>a[data-action=edit]').click(function(){
				alert("点击编辑后更多操作,可以到widgets.js内添加");
			});
			
			$('.widget-toolbar>a[data-action=delete]').click(function(){
				alert("点击删除后更多操作,可以到widgets.js内添加");
			});
			
			$('.widget-toolbar>a[data-action=save]').click(function(){
				alert("点击保存后更多操作,可以到widgets.js内添加");
			});					
		}		
	}	
})($);