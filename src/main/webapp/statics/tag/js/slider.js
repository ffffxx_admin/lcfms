window.slider=(function(){
	var obj={};
	obj.init=function(id){
		var sl=$("#slider-range-"+id);
		var str=sl.html();
		str=str.trim();
		eval("var data="+str);
		sl.html("");
		sl.addClass(data.style);	
		var h=(data.orientation=='horizontal')?'width':'height';		
		sl.empty().css(h,data.width+'px').slider({
			orientation: data.orientation,
			range: data.range,
			min: data.min,
			max: data.max,
			step: data.step,
			value: data.value,
			slide: function( event, ui ) {
				var val = parseInt(ui.value);	
				if(val<data.tmin){
					return false;
				}
				if(val>data.tmax){
					return false;
				}			
				if(!ui.handle.firstChild) {											
					$(ui.handle).append("<div class='tooltip top in' style='display:none;left:-5px;top:-35px;'><div class='tooltip-arrow'></div><div class='tooltip-inner' style='width:28px'></div></div>");							
				}								
				$(ui.handle.firstChild).show().children().eq(1).text(val);						
			}
		}).find('a').on('blur', function(){
			$(this.firstChild).hide();				
		});
	}
	return obj;
})()