(function($){
	window.spinner={
        init:function(callback){
			$("input.spinner-input").each(function(){
				var val=$(this).val();
				var min=$(this).attr("min-spinner");
				var name=$(this).attr("name");
				var id=$(this).attr("id");
				var max=$(this).attr("max-spinner");
				var step=$(this).attr("step-spinner");
				var str="";
				str+='<div class="number-spinner touch-spinner">';
					str+='<div class="input-group">';
						str+='<div class="spinner-buttons input-group-btn">';							
						str+='<button type="button" class="btn spinner-down btn-xs btn-danger">';								
						str+='<i class="fa fa-minus smaller-75"></i>';							
						str+='</button>';						
						str+='</div>';
						str+='<input type="text" readonly id="'+id+'" name="'+name+'" value="'+val+'" class="spinner-input input-mini form-control" min-spinner="'+min+'" max-spinner="'+max+'" step-spinner="'+step+'">';
						str+='<div class="spinner-buttons input-group-btn">';							
						str+='<button type="button" class="btn spinner-up btn-xs btn-success">';								
						str+='<i class="fa fa-plus smaller-75"></i>';							
						str+='</button>';						
						str+='</div>';
					str+='</div>';
				str+='</div>';	
                $(this).replaceWith(str);
			});
			$(".spinner-buttons").click(function(){
				var b=$(this).children("button");				
				if(b.hasClass("spinner-down")){
					var i=$(this).next("input");
					var val=$(i).val();
					var min=$(i).attr("min-spinner");
					var max=$(i).attr("max-spinner");
					var step=$(i).attr("step-spinner");
					if(val-step<min){
											
					}else{
						$(i).val(val-step);
						if(callback){
							callback(i);
						}
					}					
				}
				if(b.hasClass("spinner-up")){	
					var i=$(this).prev("input");
					var val=parseInt($(i).val());
					var min=$(i).attr("min-spinner");
					var max=$(i).attr("max-spinner");
					var step=parseInt($(i).attr("step-spinner"));
					if(val+step>max){

					}else{
						$(i).val(val+step);
						if(callback){
							callback(i);
						}
					}
				}
				
			});
		}		
	}	
})($);