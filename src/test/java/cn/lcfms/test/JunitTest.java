package cn.lcfms.test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.TreeSet;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import cn.lcfms.bin.BaseCache;
import cn.lcfms.bin.BaseService;
import cn.lcfms.bin.core.App;
import cn.lcfms.utils.Vardump;

@RunWith(SpringJUnit4ClassRunner.class)    
@ContextConfiguration(locations = {"classpath*:springmvc-context.xml"})    
public class JunitTest{
	
	 public void test1() throws IOException{  
		 BaseService service=App.getService("areas");
		 a1:for(int i=0;i<45700;i++){
			if(i%100==0)System.out.println(i);
			File f=new File("D:\\laocheng\\WebContent\\statics\\tag\\region\\"+i+".json");
			if(!f.exists())continue a1;
			FileReader fReader=new FileReader(f);
			BufferedReader reader=new BufferedReader(fReader);	
			StringBuffer buffer=new StringBuffer();
			String line="";
			while((line=reader.readLine())!=null){
				buffer.append(line);
			}
			reader.close();
			JSONArray array=JSONArray.fromObject(buffer.toString());
			a2:for (int j = 0; j < array.size(); j++) {
				JSONObject object = (JSONObject) array.get(j);	
				if(null==object.get("id")){
					System.out.println(i+".json,"+j+"id不存在");
					continue a2;
				}	
				int id=(int) object.get("id");	
				int deep=0;
				if(null==object.get("level")){
					System.out.println(i+".json,"+id+"level不存在");
				}else{
					deep=(int) object.get("level");	
				}
				if(null==object.get("areaName")){
					System.out.println(i+".json,"+id+"areaName不存在");
					continue a2;
				}
				String areaName=(String) object.get("areaName");	
				if(null==object.get("parentId")){
					System.out.println(i+".json,"+id+"parentId不存在");
					continue a2;
				}			
				int parentId=(int) object.get("parentId");	
				String region="";
				if(null==object.get("region")){
					//System.out.println(i+".json"+id+"region不存在");
				}else{
					region=(String) object.get("region");	
				}		
				service.setData(id,areaName,parentId,id,deep,region);
				service.insert("id","name","parent_id","sort","deep","region");
			}			
		 }
	 }  
	 
	 public void test3(){ 
		 BaseService service=App.getService("demo");
		 service.column("s",new BaseService.ColumnFormat() {			
			@Override
			public Object format(Object object) {
				String str=(String)object;
				return str+"123";
			}
		});
		service.limit(0, 10);
		List<HashMap<String, Object>> list = service.selectList(); 
		Vardump.print(list);		 
	 }
	 
	 public void test4(){
		 BaseService service=App.getService("articles");
		 service.where("article_category_id=8");
		 service.column("id","name","thumbnail","`desc`");
		 List<HashMap<String, Object>> mzcz = service.selectList();
		 Vardump.print(mzcz);
	 }
	 
	 public void test5(){
		 BaseService service = App.getService("article_categories");
		 service.column("id","topid","name");
		 BaseCache.catelist = service.selectList();	
		 TreeSet<Long> tree=new TreeSet<Long>();
		 getChildCat(2,tree);
		 Vardump.print(tree);
	 }
	 
	 private void getChildCat(long id,TreeSet<Long> tree){			 
		for(int i=0;i<BaseCache.catelist.size();i++){
			int topid=(int) BaseCache.catelist.get(i).get("topid");
			long cid=(long) BaseCache.catelist.get(i).get("id");
			if(topid==id){
				tree.add(cid);
				getChildCat(cid,tree);
			}
		}
	}
	 
	public void test6(){
	 	String filefullname="abc.fd.ff";		
		int index=filefullname.lastIndexOf(".");
		String filename="";
		String extension="";
		if(index!=-1){
			filename=filefullname.substring(0, index);
			extension=filefullname.substring(index+1);
		}else{
			filename=filefullname;
		}
		System.out.println(new Date().getTime());
		System.out.println(filename);
		System.out.println(extension);
	 }
	
	public void test7(){	
		int id=406;
		List<String> list=new ArrayList<String>();
		getAddressById(id,list);
		StringBuffer buffer=new StringBuffer();
		for(int i=list.size()-1;i>=0;i--){
			buffer.append(list.get(i));
			if(i!=0){
				buffer.append("，");
			}
		}
		System.out.println(buffer.toString());
	}
	
	private void getAddressById(long id,List<String> buffer){
		BaseService service=App.getService("areas");
		service.where("id="+id);
		HashMap<String, Object> map = service.selectMap();
		if(map==null){
			return;
		}
		long parent_id = (long) map.get("parent_id");
		if(parent_id!=0){
			buffer.add((String) map.get("name"));
			getAddressById(parent_id,buffer);
		}else{
			buffer.add((String) map.get("name"));
		}		
	}
	
	public void test8(){
		int id=76;
		BaseService service=App.getService("article_categories");
		List<HashMap<String, Object>> list = service.selectList();
		HashMap<Long, String> map=new HashMap<>();
		getCategory(id,list,map);
		Vardump.print(map);
	}
	
	private void getCategory(long id,List<HashMap<String, Object>> list,HashMap<Long, String> map) {
		for(int i=0;i<list.size();i++) {
			long cid=(long) list.get(i).get("id");
			int topid=(int) list.get(i).get("topid");
			String name=(String) list.get(i).get("name");
			if(cid==id) {
				map.put(cid, name);
				if(topid!=0) {
					getCategory(topid,list,map);
				}
			}
		}
	}
	
	public void test9() {
		Object[] objects={1,'2',"3",true,null};
    	String[][] strings={{"aaa","bbb","aaa","bbb"},{"aaa","bbb","aaa","bbb"}};
    	HashMap<Object, Object> map=new HashMap<Object, Object>();
    	map.put("aaa", 1);
    	map.put("bbb", 1.5f);
    	map.put("ccc", true);
    	map.put(5.5f, '5');
    	map.put(1, "12345");
    	map.put(true, objects);
    	map.put('g', strings);
    	map.put("obj",new T(123,"abc"));
    	List<Object> list=new ArrayList<Object>();
    	list.add("abc");
    	list.add('a');
    	list.add(true);
    	list.add(map);
    	Vardump.print(list);
	}
	
	public void test10() {
		BaseService service=App.getService("demo");
		service.setData("abc111").insert("s");
		service.transaction(()->{		
			service.setData("abc222").insert("s");
			service.setData("abc333").insert("s");		
			service.setData("abc444").insert("s");
		});
		service.setData("abc555").insert("s");
		service.transaction(()->{					
			service.setData("abc666").insert("s");		
			service.setData("abc77777777777777777").insert("s");
		});
		service.setData("abc888").insert("s");
		Vardump.print(service.getSqlBuildList());
	}
		
	public void test11() {
		BaseService service=App.getService("demo");	
		//service.column("DISTINCT s","id","i abc","t as ddd").order("a.id desc,a.s asc").limit(0, 1).selectList();
		service.column("s","SUM(i) as b").groupBy("s").having("b>15").selectList();
		Vardump.print(service.getResult());		
	}
	
	
	public void test12() {
		BaseService service=App.getService("areas");	
		service.where("id>100");
		service.order("id");
		service.leftJoin("admin").on("areas.id=admin.aid");
		service.selectPage(10, 1);
		Vardump.print(service.getResult());		
	}
	
	@Test
	public void test13() {
		 URL url = this.getClass().getResource("");
		 System.out.println("Value = " + url);
	}
}

class T{
	T(int i,String str){
		this.i=i;
		this.str=str;
	}
	private int i;
	private String str;
	public int getI() {
		return i;
	}
	public void setI(int i) {
		this.i = i;
	}
	public String getStr() {
		return str;
	}
	public void setStr(String str) {
		this.str = str;
	}	
}
