package cn.lcfms.test;

import java.io.IOException;
import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.xml.sax.SAXException;

import cn.lcfms.app.weixin.controller.ConfigController;
import cn.lcfms.app.weixin.controller.MenuController;
import cn.lcfms.bin.weixin.AesException;
import cn.lcfms.bin.weixin.WXBizMsgCrypt;
import cn.lcfms.utils.HttpUtils;

@RunWith(SpringJUnit4ClassRunner.class)    
@ContextConfiguration(locations = {"classpath*:springmvc-context.xml"}) 
@SuppressWarnings("unused")
public class ControllerTest {  
    private MockHttpServletRequest request;     
	private MockHttpServletResponse response;   
    @Autowired    
    private ConfigController config ; 
    @Autowired   
    private MenuController menu ;      
 
	public void test1() {
    	try {
			HttpUtils.postXml("<xml>abcde</xml>", "http://localhost:8080/weixin/config/notice?echostr=1");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
    
	public void test2() {
		menu.upload();
	}
	
}
